FROM registry.gitlab.com/orange-opensource/lfn/ci_cd/docker-ansible-core:2.17-alpine3.20@sha256:e43c38d8ced2bbb65c7ab1bb6c3618ba724dae770d428283f865657cd4ecf749

ARG CI_COMMIT_SHORT_SHA
ARG BUILD_DATE

LABEL org.opencontainers.image.source="https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker" \
    org.opencontainers.image.ref.name="os_infra_manager_docker by Orange" \
    org.opencontainers.image.authors="David Blaisonneau <david.blaisonneau@orange.com> \
    Sylvain Desbureaux <sylvain.desbureaux@orange.com> \
    Thierry Hardy <thierry.hardy>" \
    org.opencontainers.image.licenses=" Apache-2.0" \
    org.opencontainers.image.revision=$CI_COMMIT_SHORT_SHA

COPY ansible.cfg /etc/ansible/
ENV APP /opt/os_infra_manager_docker/
ENV COLLECTION_PATH ~/.ansible/collections
COPY requirements.txt /tmp/requirements.txt

WORKDIR $APP

COPY . $APP/

# renovate: datasource=repology depName=alpine_3_20/linux-headers versioning=loose
ENV LINUX_HEADERS_VERSION=6.6-r0
# renovate: datasource=repology depName=alpine_3_20/py3-pip versioning=loose
ENV PY3_PIP_VERSION=24.0-r2 
# renovate: datasource=repology depName=alpine_3_20/python3-dev versioning=loose
ENV PYTHON3_DEV_VERSION=3.12.9-r0
# renovate: datasource=repology depName=alpine_3_20/build-base versioning=loose
ENV BUILD_BASE_VERSION=0.5-r3

# renovate: datasource=repology depName=alpine_3_20/py3-appdirs versioning=loose
ENV PY3_APPDIRS_VERSION=1.4.4-r8
# renovate: datasource=repology depName=alpine_3_20/py3-colorama versioning=loose
ENV PY3_COLORAMA_VERSION=0.4.6-r5
# renovate: datasource=repology depName=alpine_3_20/py3-msgpack versioning=loose
ENV PY3_MSGPACK_VERSION=1.0.8-r1
# renovate: datasource=repology depName=alpine_3_20/py3-parsing versioning=loose
ENV PY3_PARSING_VERSION=3.1.2-r1
# renovate: datasource=repology depName=alpine_3_20/py3-packaging versioning=loose
ENV PY3_PACKAGING_VERSION=24.0-r1
# renovate: datasource=repology depName=alpine_3_20/py3-requests versioning=loose
ENV PY3_REQUESTS_VERSION=2.32.3-r0
# renovate: datasource=repology depName=alpine_3_20/py3-setuptools versioning=loose
ENV PY3_SETUPTOOLS_VERSION=70.3.0-r0
# renovate: datasource=repology depName=alpine_3_20/py3-six versioning=loose
ENV PY3_SIX_VERSION=1.16.0-r9

# install build dependencies
RUN apk add --no-cache --virtual build-dependencies \
  build-base="${BUILD_BASE_VERSION}" \
  linux-headers="${LINUX_HEADERS_VERSION}" \
  py3-pip="${PY3_PIP_VERSION}" \
  python3-dev="${PYTHON3_DEV_VERSION}" &&\
  #
  # install pip deps
  pip install --no-cache-dir --break-system-packages --requirement /tmp/requirements.txt && \
  #
  # install collection
  mkdir -p "$COLLECTION_PATH" && \
  git config --global --add safe.directory "$PWD/.git" &&\
  ansible-galaxy collection install "git+file://$PWD/.git" \
    -p "$COLLECTION_PATH" && \
  rm -rf "$APP/.git" && \
  rm /root/.ansible/collections/ansible_collections/community/general/plugins/modules/java_keystore.py && \
  # clean build deps
  apk del build-dependencies &&\
  #
  # add pip packages needed but removed via build-dependencies
  apk add --no-cache \
  py3-appdirs="${PY3_APPDIRS_VERSION}" \
  py3-colorama="${PY3_COLORAMA_VERSION}" \
  py3-msgpack="${PY3_MSGPACK_VERSION}" \
  py3-parsing="${PY3_PARSING_VERSION}" \
  py3-packaging="${PY3_PACKAGING_VERSION}" \
  py3-requests="${PY3_REQUESTS_VERSION}" \
  py3-setuptools="${PY3_SETUPTOOLS_VERSION}" \
  py3-six="${PY3_SIX_VERSION}" && \
  #
  # remove requirements file
  rm /tmp/requirements.txt
