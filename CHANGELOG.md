## [10.1.67](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.66...10.1.67) (2025-02-21)


### Bug Fixes

* **deps:** update dependency python-openstackclient to v7.3.1 ([4a75c99](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/4a75c9996859f0c70dc99c9c43b53adeec65025f))

## [10.1.66](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.65...10.1.66) (2025-02-15)


### Bug Fixes

* **deps:** update dependency orange-opensource/lfn/infra/infra_collection to v9.0.37 ([bcace08](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/bcace08807b4c5771bfa56885cc2746c670dccdb))

## [10.1.65](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.64...10.1.65) (2025-02-15)


### Bug Fixes

* **deps:** pin dependencies ([f32705a](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/f32705a0014f54c2b6875d7649da4898c1cbe9d4))

## [10.1.64](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.63...10.1.64) (2025-02-15)


### Bug Fixes

* **⬆️:** bump asked nova compute api version ([6174522](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/61745224e3a6419c9ff40614215f6efda05321e9))
* **deps:** update dependency alpine_3_20/python3-dev to v3.12.9-r0 ([4977842](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/4977842be1e03eafefe8496a55846008d6d1c4e2))
* **deps:** update dependency openstacksdk to v4.3.0 ([d1539a0](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/d1539a0ef05f2bcbfade29888cf891f5f010aff5))
* **deps:** update dependency orange-opensource/lfn/infra/infra_collection to v9.0.36 ([d0da180](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/d0da1803dcb1f41c63b2fb9ccfd59bd3d1ecf017))

## [10.1.63](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.62...10.1.63) (2025-01-20)


### Bug Fixes

* **deps:** update dependency openstack.cloud to >=2.4.0,<2.5.0 ([8514831](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/85148311fd585ad9787392dcd35b67c0a2991f0d))

## [10.1.62](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.61...10.1.62) (2025-01-13)


### Bug Fixes

* **deps:** update dependency python-heatclient to v4.1.0 ([5f88939](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/5f88939d6f4d68aa9559e400538ad4237bc7ffee))

## [10.1.61](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.60...10.1.61) (2025-01-09)


### Bug Fixes

* **deps:** update dependency orange-opensource/lfn/infra/infra_collection to v9.0.34 ([e60f03e](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/e60f03e240d9e56cc2a26dd75756327cf3659ec8))

## [10.1.60](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.59...10.1.60) (2025-01-07)


### Bug Fixes

* **deps:** update dependency devsec.hardening to >=10.2.0,<10.3.0 ([3ea5a8d](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/3ea5a8dec724d47682787057be625dd4780853a5))

## [10.1.59](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.58...10.1.59) (2024-12-17)


### Bug Fixes

* **deps:** update dependency orange-opensource/lfn/infra/infra_collection to v9.0.32 ([9bf4630](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/9bf4630be2ba903b9edd076eae81c46f21abb7a6))

## [10.1.58](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.57...10.1.58) (2024-12-16)


### Bug Fixes

* **deps:** update dependency openstacksdk to v4.2.0 ([b6beca5](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/b6beca53ddf134ad0395fb56a2c3f22e43d7043d))

## [10.1.57](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.56...10.1.57) (2024-12-13)


### Bug Fixes

* **deps:** update dependency orange-opensource/lfn/infra/infra_collection to v9.0.31 ([5874392](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/5874392c330e134448df33e91799c4b356ed0153))

## [10.1.56](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.55...10.1.56) (2024-12-13)


### Bug Fixes

* **deps:** update dependency alpine_3_20/python3-dev to v3.12.8-r1 ([1fd7e41](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/1fd7e41ca640dd147e4e61d2e8fafb0de5d1a333))

## [10.1.55](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.54...10.1.55) (2024-11-30)


### Bug Fixes

* **deps:** update dependency python-octaviaclient to v3.9.0 ([9981078](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/9981078af2c0300238b0ec40e8ebe082a1cef0a7))

## [10.1.54](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.53...10.1.54) (2024-11-29)


### Bug Fixes

* **deps:** update dependency openstack.cloud to >=2.3.0,<2.4.0 ([accf4de](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/accf4de011d502470cca24fecd54919d68c66bd5))

## [10.1.53](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.52...10.1.53) (2024-11-29)


### Bug Fixes

* **deps:** update docker-ansible-core docker tag to v2.17 and alpine 3.20 ([83bde48](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/83bde48bdfa5a4a5d0766c66d858e81ae3c1d74f))

## [10.1.52](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.51...10.1.52) (2024-11-27)


### Bug Fixes

* **deps:** update galaxy packages ([629f2a4](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/629f2a41486be1d5c02cf4ce319a4df323e05606))

## [10.1.51](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.50...10.1.51) (2024-10-30)


### Bug Fixes

* **deps:** update dependency python-openstackclient to v7.2.1 ([39170ec](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/39170ec5b4a51b6f2eec1b6f4b03cd6cdb8489d4))

## [10.1.50](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.49...10.1.50) (2024-10-21)


### Bug Fixes

* **deps:** update dependency orange-opensource/lfn/infra/infra_collection to v9.0.26 ([545ce24](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/545ce24233435f36b08c785e8e5116b93d90044b))

## [10.1.49](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.48...10.1.49) (2024-10-21)


### Bug Fixes

* **deps:** update python packages ([904c337](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/904c337510b23e75b52b3d43a5c2481026d5e484))

## [10.1.48](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.47...10.1.48) (2024-09-26)


### Bug Fixes

* change python shebang ([042d11d](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/042d11df0c51cea24753c5462ef60ae24629b706))

## [10.1.47](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.46...10.1.47) (2024-09-26)


### Bug Fixes

* **deps:** update dependency orange-opensource/lfn/infra/infra_collection to v9.0.24 ([9bb5be0](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/9bb5be0d873db4a7809232a9d2bc52d7aea291b0))

## [10.1.46](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.45...10.1.46) (2024-09-25)


### Bug Fixes

* **deps:** update dependency devsec.hardening to v10 ([24a170a](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/24a170a31ec9b54f6cc0054a5cb3123717efa0ee))
* **deps:** update galaxy packages ([f2f8e0c](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/f2f8e0c5e11bdade858c49bedb8dbef253c25019))

## [10.1.45](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.44...10.1.45) (2024-09-24)


### Bug Fixes

* **deps:** update alpine packages ([8e42fc2](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/8e42fc29fb9e56def0c3e21193e0416f1d538a96))
* **deps:** update python packages ([27233b3](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/27233b3269b10574bc577bf4da3fa917508c13ca))

## [10.1.44](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.43...10.1.44) (2024-07-09)


### Bug Fixes

* **deps:** update dependency jsonschema to v4.23.0 ([9f58b41](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/9f58b417ec96875f1d88ffd620256fcae7adfe31))

## [10.1.43](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.42...10.1.43) (2024-07-04)


### Bug Fixes

* **deps:** update dependency openstacksdk to v3.2.0 ([4a0f28f](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/4a0f28ffdb4d04088e232d5f784f4d1decd082f5))

## [10.1.42](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.41...10.1.42) (2024-06-18)


### Bug Fixes

* **deps:** update dependency orange-opensource/lfn/infra/infra_collection to v9.0.20 ([cbb30df](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/cbb30dfb49b944cf7512ac306bf216aa9aa73334))

## [10.1.41](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.40...10.1.41) (2024-06-14)


### Bug Fixes

* **deps:** update dependency ansible.netcommon to v7 ([6c18fc9](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/6c18fc916d2b8f61c40a6672c343fb204cd613e7))

## [10.1.40](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.39...10.1.40) (2024-05-23)


### Bug Fixes

* **deps:** update dependency orange-opensource/lfn/infra/infra_collection to v9.0.19 ([6f5ccd3](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/6f5ccd366b15c4ab47ecd4b063f97a63574f6bb3))

## [10.1.39](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.38...10.1.39) (2024-05-07)


### Bug Fixes

* **deps:** update dependency jsonschema to v4.22.0 ([034a5ab](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/034a5abe3270b6a5ea70cf5f3611d1345aa6f7a0))

## [10.1.38](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.37...10.1.38) (2024-04-29)


### Bug Fixes

* **deps:** update dependency orange-opensource/lfn/infra/infra_collection to v9.0.18 ([d3d1a59](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/d3d1a59b6a89a4792c673393848a69f362c2aaa0))

## [10.1.37](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.36...10.1.37) (2024-04-18)


### Bug Fixes

* **deps:** update dependency openstacksdk to v3.1.0 ([b1f3b5a](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/b1f3b5af844b4c5746f4c4a71c6f16828505bfc3))

## [10.1.36](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.35...10.1.36) (2024-04-15)


### Bug Fixes

* **deps:** update dependency ansible.netcommon to >=6.1.0,<6.2.0 ([6d7d116](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/6d7d116123dd9ce8ed9f1fded3883affb5862deb))

## [10.1.35](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.34...10.1.35) (2024-03-26)


### Bug Fixes

* **deps:** update dependency orange-opensource/lfn/infra/infra_collection to v9.0.17 ([d64dc12](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/d64dc12ed58370170d8a825c9ff1b9239dd1db86))

## [10.1.34](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.33...10.1.34) (2024-03-26)


### Bug Fixes

* **deps:** update dependency alpine_3_17/python3-dev to v3.10.14-r1 ([c74d90c](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/c74d90c97650029519d15580f25c4431ed88895e))

## [10.1.33](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.32...10.1.33) (2024-03-19)


### Bug Fixes

* **deps:** update dependency python-openstackclient to v6.6.0 ([3b514f2](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/3b514f2252c8964bec3159c29f75cafbbc7c3895))

## [10.1.32](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.31...10.1.32) (2024-03-13)


### Bug Fixes

* **deps:** update python packages ([974b33a](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/974b33a9557ef79f271d356d61bad3da9c6468e3))

## [10.1.31](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.30...10.1.31) (2024-03-13)


### Bug Fixes

* **deps:** update dependency orange-opensource/lfn/infra/infra_collection to v9.0.16 ([a56892d](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/a56892d2c65bf811cc988096483189b9cc69b2fa))

## [10.1.30](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.29...10.1.30) (2024-2-2)


### Bug Fixes

* **deps:** update dependency python-openstackclient to v6.5.0 ([64806d0](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/64806d054023196816cc43783dff8ac660cf1e06))

## [10.1.29](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.28...10.1.29) (2024-1-31)


### Bug Fixes

* **deps:** update dependency orange-opensource/lfn/infra/infra_collection to v9.0.15 ([fc921ca](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/fc921ca5177428f70713a7a7157a487e6c36b557))

## [10.1.28](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.27...10.1.28) (2024-1-22)


### Bug Fixes

* **deps:** update dependency jsonschema to v4.21.1 ([f824653](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/f824653c8aeca27af27e08632a6eb3da8ac3ac0f))

## [10.1.27](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.26...10.1.27) (2024-1-17)


### Bug Fixes

* **deps:** update dependency jsonschema to v4.21.0 ([3ccc056](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/3ccc0562b5581945d715d1a3e4f2e05ffe51ad70))

## [10.1.26](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.25...10.1.26) (2024-1-15)


### Bug Fixes

* **deps:** update dependency openstacksdk to v2.1.0 ([5dca6ff](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/5dca6ffb8f7222475b5b1bdbbfedd2c5c0754a60))

## [10.1.25](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.24...10.1.25) (2024-01-11)


### Bug Fixes

* **deps:** update dependency python-heatclient to v3.4.0 ([03c447b](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/03c447b93ed646aa1ebefaa1b16b116a8903ad35))

## [10.1.24](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.23...10.1.24) (2024-01-09)


### Bug Fixes

* **deps:** update dependency orange-opensource/lfn/infra/infra_collection to v9.0.14 ([4ee884d](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/4ee884d28c47110f1577439c8ba7e3b62467481b))

## [10.1.23](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.22...10.1.23) (2023-12-12)


### Bug Fixes

* **deps:** update dependency python-openstackclient to v6.4.0 ([6076c35](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/6076c357d970c47a7df7188adf49d2e0b006730c))

## [10.1.22](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.21...10.1.22) (2023-12-06)


### Bug Fixes

* **🔧👽️🩹:** make add allowed ip pair work again ([cef20c0](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/cef20c012a9c59bc39c1bf468243bd34ba24621d))

## [10.1.21](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.20...10.1.21) (2023-12-06)


### Bug Fixes

* **deps:** update dependency ansible.netcommon to v6 ([c09d881](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/c09d881668a718588cbb0db246d73967adae12ea))

## [10.1.20](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.19...10.1.20) (2023-12-06)


### Bug Fixes

* **deps:** update galaxy packages ([c86e980](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/c86e980397a3a808bed205404bc1d35f5e797fd3))

## [10.1.19](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.18...10.1.19) (2023-11-20)


### Bug Fixes

* **deps:** update dependency python-octaviaclient to v3.6.0 ([3ebba74](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/3ebba74bc3756ca6e44c52b01c4f6934e8bd2360))

## [10.1.18](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.17...10.1.18) (2023-11-20)


### Bug Fixes

* **deps:** update dependency devsec.hardening to v9 ([ad74bf5](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/ad74bf55e1479cc9198a1a1d74521b3650bd2134))

## [10.1.17](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.16...10.1.17) (2023-11-17)


### Bug Fixes

* **deps:** update dependency jsonschema to v4.20.0 ([773c56e](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/773c56ee43e5699065aacc3c0d01b03a1aed74df))

## [10.1.16](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.15...10.1.16) (2023-11-15)


### Bug Fixes

* **⏲️:** set more timers and make them configurable during lb creation ([ebf42fd](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/ebf42fdebee6471537a3c2c5f336c88197189c48))

## [10.1.15](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.14...10.1.15) (2023-11-13)


### Bug Fixes

* **🔧:** put some pause between lb members creation ([74c05b4](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/74c05b42ba10249b97c29a90451b5d70d8140571))

## [10.1.14](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.13...10.1.14) (2023-11-13)


### Bug Fixes

* **deps:** update dependency orange-opensource/lfn/infra/infra_collection to v9.0.12 ([1fdf16e](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/1fdf16ee2685e0e44500f838062f56e0e1825754))

## [10.1.13](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.12...10.1.13) (2023-11-13)


### Bug Fixes

* **deps:** update dependency ansible.netcommon to >=5.3.0,<5.4.0 ([479d130](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/479d130f38ef23c3371ab050cc0132d425dd195f))

## [10.1.12](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.11...10.1.12) (2023-11-13)


### Bug Fixes

* **👽️:** role _member_ has been renamed ([7a047ff](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/7a047ff7a2fac6376261a86f4263e9ba7c81a4f3))

## [10.1.11](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.10...10.1.11) (2023-10-31)


### Bug Fixes

* **deps:** update dependency jsonschema to v4.19.2 ([212093a](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/212093a49ebba600c1fc861f8594eb022e7ab240))

## [10.1.10](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.9...10.1.10) (2023-10-24)


### Bug Fixes

* **deps:** update dependency python-octaviaclient to v3.5.1 ([99185c9](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/99185c931bb5e75459176a7b61e3353ec18a2733))

## [10.1.9](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.8...10.1.9) (2023-10-20)


### Bug Fixes

* **deps:** update dependency openstacksdk to v2 ([1473901](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/1473901cab8d29b602def1c9f627ac5e0aa77f98))

## [10.1.8](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.7...10.1.8) (2023-09-22)


### Bug Fixes

* **deps:** update dependency jsonschema to v4.19.1 ([9b0d3db](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/9b0d3dbba5838b07bbb3d1d7ed0ca3d259a3f9ab))

## [10.1.7](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.6...10.1.7) (2023-09-18)


### Bug Fixes

* **deps:** update dependency python-openstackclient to v6.3.0 ([08f97b3](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/08f97b380d5a2f88ce28ee50d7668c3e6b15ccba))

## [10.1.6](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.5...10.1.6) (2023-09-13)


### Bug Fixes

* **deps:** update dependency orange-opensource/lfn/infra/infra_collection to v9.0.11 ([bead84b](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/bead84b2557ea1fe0c63bfbfb2effc8f763a3db8))

## [10.1.5](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.4...10.1.5) (2023-09-13)


### Bug Fixes

* **deps:** update dependency ansible.netcommon to >=5.2.0,<5.3.0 ([66f7b21](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/66f7b21fe0fbe75f56e22e79da3e95f0ba7e7f47))

## [10.1.4](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.3...10.1.4) (2023-08-31)


### Bug Fixes

* **deps:** update python packages ([88c6e95](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/88c6e9568183ebe7ffd44ea43f408639e6449409))

## [10.1.3](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.2...10.1.3) (2023-08-31)


### Bug Fixes

* **deps:** update dependency orange-opensource/lfn/infra/infra_collection to v9.0.10 ([a650f79](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/a650f792499f9d37677bae6507a15f9079bc4740))

## [10.1.2](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.1...10.1.2) (2023-08-29)


### Bug Fixes

* **deps:** update python packages ([bd33ac4](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/bd33ac469af26412465c2f974d7fe7cf64b0735d))

## [10.1.1](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.1.0...10.1.1) (2023-08-04)


### Bug Fixes

* **deps:** update dependency jsonschema to v4.18.6 ([42e8deb](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/42e8debf354a2baa369c91af67668a4b80b1be65))

# [10.1.0](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/10.0.0...10.1.0) (2023-07-24)


### Features

* **deps:** update galaxy packages and their dependencies ([c71d895](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/c71d895e0f86662b4df75a1b9594e9c4198fad2c))

# [10.0.0](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/9.2.2...10.0.0) (2023-07-24)


### Features

* **🎨:** use virtualenv when not local ([39473cb](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/39473cb951f3bb653d9482f29012f179127d9d4d))


### BREAKING CHANGES

* **🎨:** when using a jumphost, i.e. when you don't launch
Openstack command directly from a container, you must ensure the
virtualenv is well set before using the different roles. See the
different playbooks for example on what to do.

Signed-off-by: Sylvain Desbureaux <sylvain.desbureaux@orange.com>

## [9.2.2](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/9.2.1...9.2.2) (2023-07-18)


### Bug Fixes

* **deps:** update dependency jsonschema to v4.18.4 ([5f21108](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/5f21108c689ffefc318f8e3bc27bd10527f405ee))

## [9.2.1](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/9.2.0...9.2.1) (2023-07-18)


### Bug Fixes

* **deps:** update dependency orange-opensource/lfn/infra/infra_collection to v9.0.9 ([80b709b](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/80b709bb64105a87dffa62343e5116cc6a6b3c95))

# [9.2.0](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/9.1.18...9.2.0) (2023-07-18)


### Features

* **✨:** allow to set Floating IP on VMs ([33be22d](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/33be22ddb3d7e1d6b4ac94ab8c4539e7d07110ab))

## [9.1.18](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/9.1.17...9.1.18) (2023-06-19)


### Bug Fixes

* **deps:** update dependency alpine_3_17/python3-dev to v3.10.12-r0 ([be74c13](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/commit/be74c13afd769e1ad2acd9e79fa185f4da83361d))

## [9.1.17](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/compare/9.1.16...9.1.17) (2023-05-20)


### Bug Fixes

* **deps:** update dependency python-heatclient to v3.3.0 ([e5fe7e0](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/-/commit/e5fe7e082d247f56b8bdac3c1a47e7604895f215))

## [9.1.16](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/-/compare/9.1.15...9.1.16) (2023-05-17)


### Bug Fixes

* **🩹:** make security group rules idempotent ([9b6e11b](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/-/commit/9b6e11b2b0df5d3ad2dbb1dbcfaa15ed6faeb32c))

## [9.1.15](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/-/compare/9.1.14...9.1.15) (2023-05-16)


### Bug Fixes

* **deps:** update dependency orange-opensource/lfn/infra/infra_collection to v9.0.7 ([5fd25cb](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/-/commit/5fd25cb040dee453ef1ce82f101e761daeb4ed80))

## [9.1.14](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/-/compare/9.1.13...9.1.14) (2023-05-05)


### Bug Fixes

* **deps:** update dependency orange-opensource/lfn/infra/infra_collection to v9.0.6 ([5a02ce4](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/-/commit/5a02ce4add76c965be3421f7b9c5736357777819))

## [9.1.13](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/-/compare/9.1.12...9.1.13) (2023-05-04)


### Bug Fixes

* **deps:** update dependency alpine_3_17/python3-dev to v3.10.11-r0 ([e62a33e](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/-/commit/e62a33ef0b0f89f7f1b42acc83aeda6cbdb9091e))

## [9.1.12](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/-/compare/9.1.11...9.1.12) (2023-03-28)


### Bug Fixes

* **deps:** update dependency orange-opensource/lfn/infra/infra_collection to v9.0.5 ([54fac96](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/-/commit/54fac9656a30131b3b98993d2b71c32b45008de2))

## [9.1.11](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/-/compare/9.1.10...9.1.11) (2023-03-01)


### Bug Fixes

* **deps:** update dependency orange-opensource/lfn/infra/infra_collection to v9.0.4 ([1550104](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/-/commit/155010475125061456fcaac78803e5a6c48d7acc))

## [9.1.10](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/-/compare/9.1.9...9.1.10) (2023-02-25)


### Bug Fixes

* **deps:** update python packages ([6b1efab](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/-/commit/6b1efab95f1757cd0cf661408756bbd152aa6aa0))

## [9.1.9](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/-/compare/9.1.8...9.1.9) (2023-02-23)


### Bug Fixes

* **🔧:** allow to configure quotas for server groups ([db10d3f](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/-/commit/db10d3f0ec819dc9890c165ab3afe22e77a4d992))

## [9.1.8](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/-/compare/9.1.7...9.1.8) (2023-02-21)


### Bug Fixes

* **deps:** update dependency python-octaviaclient to v3.3.0 ([2adfd3f](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/-/commit/2adfd3fc75e24041782ac87451be7f28cb9a23a7))

## [9.1.7](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/-/compare/9.1.6...9.1.7) (2023-02-21)


### Bug Fixes

* **🛂:** add nosuid to `/var` ([ac4ec6d](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/-/commit/ac4ec6ddd712ecfd31f10fe56203e1b3fe7a44a2))

## [9.1.6](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/-/compare/9.1.5...9.1.6) (2023-02-21)


### Bug Fixes

* **deps:** update dependency alpine_3_17/python3-dev to v3.10.10-r0 ([1d97f54](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/-/commit/1d97f54a16d0d5075e734f52d16bdc4f0f770ab5))

## [9.1.5](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/-/compare/9.1.4...9.1.5) (2023-02-04)


### Bug Fixes

* **deps:** update dependency orange-opensource/lfn/infra/infra_collection to v9.0.3 ([5f81ad8](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/-/commit/5f81ad890fcc1f8c03206dd662f111e1d96e26b1))

## [9.1.4](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/-/compare/9.1.3...9.1.4) (2023-02-02)


### Bug Fixes

* **deps:** update dependency openstacksdk to v0.62.0 ([9e9a10b](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/-/commit/9e9a10bd2bf24ea41a871e090513858f00d340f9))

## [9.1.3](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/-/compare/9.1.2...9.1.3) (2023-02-02)


### Bug Fixes

* **🩹:** check log audit path exists as root ([dbb6715](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/-/commit/dbb6715d56d2e7a755533c89df23574ef2778697))

## [9.1.2](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/-/compare/9.1.1...9.1.2) (2023-01-26)


### Bug Fixes

* **deps:** update python packages ([305b8ab](https://gitlab.com/Orange-OpenSource/lfn/infra/os_infra_manager_docker/-/commit/305b8ab2bbd68f1afa36bcb3298b2574b6272365))
