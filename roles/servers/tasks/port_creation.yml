---
- name: empty nic list for node
  ansible.builtin.set_fact:
    net_list: []
    use_only_provider: false

- name: generate nic list for node on non provider network
  ansible.builtin.set_fact:
    net_list:
      "{{ net_list +
          [{'net-name': item.network ~ os_infra_network_identifier}] }}"
  loop: "{{ node.interfaces }}"
  loop_control:
    label: "{{ node.name }}-{{ item.name }}"
  when: node.node.vendor == 'openstack' and
    (os_infra_net_config[item.network]['provider'] is not defined or
      not(os_infra_net_config[item.network]['provider']|bool))

- name: add port on nic list for node on provider network
  ansible.builtin.set_fact:
    net_list:
      "{{ net_list +
          [{'port-name':
              node.name ~ '-' ~ item.name ~ os_infra_network_identifier}] }}"
  loop: "{{ node.interfaces }}"
  loop_control:
    label: "{{ node.name }}-{{ item.name }}"
  when: node.node.vendor == 'openstack' and
    os_infra_net_config[item.network]['provider'] is defined and
    os_infra_net_config[item.network]['provider']|bool

- name: create port with fixed ip address
  openstack.cloud.port:
    state: present
    cloud: "{{ os_infra_user_name }}"
    name: "{{ node.name }}-{{ item.name }}{{ os_infra_network_identifier }}"
    network: "{{ item.network }}"
    security_groups: "{{ os_infra_security_groups_binding[node.name] }}"
    fixed_ips:
      - ip_address: "{{ item.address }}"
  loop: "{{ node.interfaces }}"
  loop_control:
    label: "{{ node.name }}-{{ item.name }} -> {{ item.network }}"
  when: node.node.vendor == 'openstack' and
    os_infra_net_config[item.network]['provider'] is defined and
    os_infra_net_config[item.network]['provider']|bool

- name: add allowed ip pairs
  openstack.cloud.port:
    state: present
    cloud: "{{ os_infra_user_name }}"
    name: "{{ node.name }}-{{ item.nic }}{{ os_infra_network_identifier }}"
    network: >-
      {{ node.interfaces | selectattr('name', 'eq', item.nic) |
      map(attribute='network') | list | first }}
    allowed_address_pairs: "{{ item.pairs }}"
  loop: "{{ os_infra_allowed_ip_pairs }}"
  loop_control:
    label: "{{ node.name }}-{{ item.nic }}"
  when: os_infra_allowed_ip_pairs is defined and
    node.node.vendor == 'openstack' and
    item.group in os_infra_nodes_roles[node.name]

- name: add node nic list to global one
  ansible.builtin.set_fact:
    os_infra_nics_list:
      "{{ os_infra_nics_list|default({}) |
          combine({node.name: net_list}) }}"
  when: node.node.vendor == 'openstack'
