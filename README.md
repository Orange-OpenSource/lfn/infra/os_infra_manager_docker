# OS Infra manager collection

## Purpose

Deploy Openstack project and servers

It can be run by importing the [roles](docs/roles.md) into your playbook,
launching the [playbooks](docs/playbooks.md) created here or using the
[CI template](gitlab-ci.template.yml) in a broader chained ci pipeline.

## Input

### Optional configuration files

- infrastructure file (`vars/pdf.yml` per default): give the [description of
  servers](docs/generic/server_description.md)
- property files(`vars/idf.yml` per default): give the [description of
  networks](docs/generic/networks_description.md) and overrided values for the
  different parameters needed to be overridden
- `vars/openstack_openrc`: the admin openrc for openstack creds
- `inventory/jumphost`: the ansible inventory for the jumphost

all playbook configuration documentation is available here

### Optional environment variables when using CI template

On top of [environment variables seen in inventory](
docs/playbooks/default_values_in_inventory.md), here are the following
environment variables that can be used and control gitlab in the gitlab ci
template:

- `ONLY_CLEAN`: perform only the cleaning of the resources
- `ADMIN`: tasks with administrator clearance will be performed (cleaning,
  user creation, project creation, network creation with admin clearance, ...)
- `CLEAN`: clean the resources at start of the pipeline
- `PARALLEL_RUNS`: launch server creation in parallel (3 slices)
- `ENABLE_HARDENING`: launch hardening of servers

## Output

The following artifacts are created:

- `inventory/infra`: the inventory of the deployed infra
- `vars/user_cloud.yml`: the user cloud config
- `vars/openstack_user_openrc`: the user openrc
