# Available roles in this collection

Each role has a specific documentation page:

* [admin_clean](roles/admin_clean.md)
* [cloud](roles/cloud.md)
* [configure_disks](roles/configure_disks.md)
* [external_requirements](roles/external_requirements.md)
* [flavor](roles/flavor.md)
* [inventory](roles/inventory.md)
* [network](roles/network.md)
* [override_vars](roles/override_vars.md)
* [project](roles/project.md)
* [security](roles/security.md)
* [server_groups](roles/server_groups.md)
* [servers](roles/servers.md)
* [set_keys](roles/set_keys.md)
* [set_proxy](roles/set_proxy.md)
* [user](roles/user.md)
* [user_clean](roles/user_clean.md)
