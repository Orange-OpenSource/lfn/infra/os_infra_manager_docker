# Available playbooks in this collection

The default values in inventory are described
[here](playbooks/default_values_in_inventory.md).

Each playbook has a specific documentation page:

* [admin_clean](playbooks/admin_clean.md)
* [cloud](playbooks/cloud.md)
* [configure_disks](playbooks/configure_disks.md)
* [flavor](playbooks/flavor.md)
* [hardening](playbooks/hardening.md)
* [inventory](playbooks/inventory.md)
* [network](playbooks/network.md)
* [project](playbooks/project.md)
* [security](playbooks/security.md)
* [server_groups](playbooks/server_groups.md)
* [servers](playbooks/servers.md)
* [set_keys](playbooks/set_keys.md)
* [set_proxy](playbooks/set_proxy.md)
* [user](playbooks/user.md)
* [user_clean](playbooks/user_clean.md)
