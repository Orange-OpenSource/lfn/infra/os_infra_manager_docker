# Load balancers description

The primary intent of os_infra_manager is to create virtual machines as desired by
the person launching it.

But some load balancers may also be needed.

Therefore, a list named `os_infra_load_balancers` allows to describe and create
such resources. This will create basic loadbalancers (same ports for upstream
and downstream, one pool per listener, only created servers can be used, ...).

Each item of the list is a dictionnary describing the load balancer and its
listeners.

## Load balancer description

Each load balancer will be described with the following variables:

| Variable         | Purpose                       | Type       | Mandatory |
|------------------|-------------------------------|------------|-----------|
| `network`        | the network to use            | string     | Yes       |
| `name`           | the name of the load balancer | string     | Yes       |
| `public`         | Do we add a floating IP?      | string     | No        |
| `listeners`      | the listeners to create       | listener[] | Yes       |

If `public` is not set, it's assumed to be `true`.
The `network` must have a router connecting it to `os_infra_public_net_name`,
which must also be declared when `public` is set to `true` (or unset).

## Listener description

A load balancer is only relevant when listeners are attached to it. A listener
will listen to a specific port, on a specific protocol and send the traffic to
downstream servers.

Optional health monitor can be added.

Here's the listener configuration variables:

<!-- markdownlint-disable line-length -->
| Variable                  | Purpose                                                  | Type    | Mandatory | Default value      | Allowed values                                                                   |
|---------------------------|----------------------------------------------------------|---------|-----------|--------------------|----------------------------------------------------------------------------------|
| `name`                    | the name of the listener                                 | string  | Yes       | N/A                | any string                                                                       |
| `protocol`                | the protocol to use                                      | string  | Yes       | N/A                | [`HTTP`, `HTTPS`, `SCTP`, `TCP`, `UDP`]                                          |
| `port`                    | the port to listen on                                    | integer | Yes       | N/A                | >=1, <=65535                                                                     |
| `algorithm`               | the load balancing algorithm                             | string  | No        | `ROUND_ROBIN`      | [`LEAST_CONNECTIONS`, `ROUND_ROBIN`, `SOURCE_IP`]                                |
| `health`                  | Do we create an health monitor                           | boolean | No        | Not set            | `true` or `false`                                                                |
| `health_delay`            | delay between each health checks                         | integer | No        | `10` seconds       | >=1                                                                              |
| `health_code`             | expected response code                                   | integer | No        | `200`              | an HTTP code, a list (`200,301`), a range (`200-203`) or a mix                   |
| `health_http_method`      | HTTP method to use                                       | string  | No        | `GET`              | [`GET`, `CONNECT`, `DELETE`, `HEAD`, `OPTIONS`, `PATCH`, `POST`, `PUT`, `TRACE`] |
| `health_max_retries`      | number of successful checks before setting it ONLINE     | integer | No        | `3`                | >=1                                                                              |
| `health_max_retries_down` | number of allowed check failures before setting it ERROR | integer | No        | `3`                | >=1, <=10                                                                        |
| `health_resp_timeout`     | timeout in seconds                                       | integer | No        | `5` seconds        | >=1, must be less than `health_delay`                                            |
| `health_type`             | the protocol to use for health checks                    | string  | No        | `"{{ protocol }}"` | [`HTTP`, `HTTPS`, `PING`, `SCTP`, `TCP`, `TLS-HELLO`, `UDP-CONNECT`]             |
| `health_url_path`         | the url path when using HTTP / HTTPS                     | string  | No        | '/'                | any valid url path                                                               |
<!-- markdownlint-enable line-length -->

## Binding server to listener

In order to bind to the server, the targeted servers must fulfill 2 conditions:

* they belong to a group named `"lb_{{ loadbalancer.name }}"`,
* they have a port (auto created or not) on `"{{ loadbalancer.network }}"`

## Examples

### Simplest example

This will create a load balancer listening on port 80 and transferring to
control01 port 80.

```yaml
os_infra_net_config:
  admin:
    network: 192.168.64.0
    mask: 24
    gateway: 192.168.64.1
    dns: 192.168.64.1

os_infra_public_net_name: ext-net-no-internet

os_infra_nodes_roles:
  control01: [kube-master, lb_controller]
  compute01: [kube-node]
  compute02: [kube-node]

os_infra_load_balancer:
  - name: controller
    network: admin
    listeners:
      - name: api
        protocol: HTTP
        port: 80
```

### More complex example

This will create 2 load balancers:

* First will listen on HTTPS on ports 6443 and 9345 and transfer to control01,
  control02 and control03 if their respective healthchecks are OK;
* Second will listen on HTTP port 80 and HTTPS port 443 on compute01 and
  compute02 if their respective healthchecks are OK.

```yaml
os_infra_net_config:
  admin:
    network: 192.168.64.0
    mask: 24
    gateway: 192.168.64.1
    dns: 192.168.64.1

os_infra_public_net_name: ext-net-no-internet

os_infra_nodes_roles:
  control01: [kube-master, lb_controller]
  control02: [kube-master, lb_controller]
  control03: [kube-master, lb_controller]
  compute01: [kube-node, lb_nginx]
  compute02: [kube-node, lb_nginx]

os_infra_load_balancer:
  - name: controller
    network: admin
    listeners:
      - name: api
        protocol: HTTPS
        port: 6443
        health: true
        health_url_path: '/readyz'
        health_code: 200,401
      - name: supervisor
        protocol: HTTPS
        port: 9345
        health: true
        health_url_path: '/ping'
        health_code: 200
  - name: nginx
    network: admin
    listeners:
      - name: api
        protocol: HTTP
        port: 80
        health: true
        health_url_path: '/'
        health_code: 200,401,404
      - name: supervisor
        protocol: HTTPS
        port: 443
        health: true
        health_url_path: '/'
        health_code: 200,401,404
```
