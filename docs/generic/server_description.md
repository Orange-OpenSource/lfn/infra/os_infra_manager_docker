# Servers description

Primary intent of os_infra_manager is to create virtual machines as desired by
the person launching it.

Therefore, a description of what's intended is needed and enforced.

This description relies on "Platform Description Format" created by
[OPNV](https://www.opnfv.org/).

As this format was done for baremetal server, only a subset of it is used by
os_infra_manager.

## Expected format

os_infra_manager will expect a list of vms to be created named `nodes`.

Each VM will need to comply to the following schema:

| Variable         | Purpose                                | Type        |
|------------------|----------------------------------------|-------------|
| `name`           | the name of the server                 | string      |
| `public_ip`      | the public IP of the server (optional) | string      |
| `node`           | the description of the server          | dictionary  |
| `disks`          | the list of the disks                  | list        |
| `interfaces`     | the list of network interfaces         | list        |

### Node description

for all nodes we want to create, the description will need to comply to the
following schema:

| Variable         | Purpose                                | Type        |
|------------------|----------------------------------------|-------------|
| `type`           | the server type (set to `virtual`)     | string      |
| `vendor`         | the vendor name (set to `openstack`)   | string      |
| `model`          | the image name for boot                | list        |

In order to choose or create the flavor we want to use, one of the following
set of values must be given:

| Variable         | Purpose                                | Type        |
|------------------|----------------------------------------|-------------|
| `flavor`         | the flavor to use                      | string      |

or

| Variable         | Purpose                                | Type        |
|------------------|----------------------------------------|-------------|
| `cpus`           | the number of vCPU                     | string      |
| `memory`         | the memory size in Gigabyte (ex: `1G`) | string      |

A disk size, which is retrieved in node disk list will also be needed in that
case.

if `flavor` is provided, it'll have precedence on the rest of the description.

### Node disks

for all nodes we want to create, the `disks` list will be needed to create the
boot volume and all other volumes we want to have

Each disk will be described with the following variables:

| Variable         | Purpose                                | Type        |
|------------------|----------------------------------------|-------------|
| `name`           | the name of the disk                   | string      |
| `disk_capacity`  | the disk size in Gigabyte (ex: `1G`)   | string      |
| `disk_type`      | disk type (set to `qcow2`)             | string      |
| `disk_interface` | the disk interface                     | string      |

#### Notes

* The `name` is used to "discover" purpose of the disk (see
  [configure_disk role](../roles/configure_disks.md) for actual use).
* The `disk_interface` is used to choose a volume type in OpenStack. If the
  volume type is unknown, default one will be used
* At least one disk is needed when a flavor is created as the size is used to
  create the flavor
* First disk is always "boot" disk

### Node interfaces

for all nodes we want to create, the `interfaces` list will be needed to create
the relevant network port on the right networks.

Each interface will be described with the following variables:

| Variable         | Purpose                                | Type        |
|------------------|----------------------------------------|-------------|
| `name`           | the name of the interface (not used)   | string      |
| `network`        | the network name where to plug it      | string      |

If the network is a provider network (see
[network description](./networks_description.md) for explanation), the following
variable will have also to be given:

| Variable         | Purpose                                | Type        |
|------------------|----------------------------------------|-------------|
| `address`        | the IP address to use                  | string      |

## Examples

### Simplest example

this describes a VM named `server-by-os-infra-manager` using flavor `myFlavor`,
image `debian-10` and attached to `admin` network

```yaml
nodes:
- name: server
    node:
      type: virtual
      vendor: openstack
      model: debian-10
      flavor: myFlavor
    disks: []
    interfaces:
      - name: nic0
        network: admin
```

### One VM with flavor creation

Same as previous but instead of using a flavor, we describe the need of CPUs,
memory and disk.

```yaml
- name: server
    node:
      type: virtual
      vendor: openstack
      model: debian-10
      cpus: 1
      memory: 1G
    disks:
      - name: disk1
        disk_capacity: 10G
        disk_type: qcow2
        disk_interface: ide
    interfaces:
      - name: nic0
        network: admin
```

### More complex example

this describes 2 VMs:

* first one has 2 disks and two interfaces (one being on a provider network)
* second one has 3 disks and one interface

```yaml
nodes:
  - name: server01
    node:
      type: virtual
      vendor: openstack
      model: debian-10
      flavor: B1.small
    disks:
      - name: disk1
        disk_capacity: 10G
        disk_type: qcow2
        disk_interface: ide
      - name: disk-logs
        disk_capacity: 10G
        disk_type: qcow2
        disk_interface: ssd
    interfaces:
      - name: nic0
        network: provider_net
        address: 172.20.60.60
      - name: nic1
        network: normal_net
  - name: server02
    node:
      type: virtual
      vendor: openstack
      model: debian-10
      flavor: B1.small
    disks:
      - name: disk1
        disk_capacity: 10G
        disk_type: qcow2
        disk_interface: ide
      - name: disk-logs
        disk_capacity: 10G
        disk_type: qcow2
        disk_interface: ssd
      - name: disk-tmp
        disk_capacity: 30G
        disk_type: qcow2
        disk_interface: ide
    interfaces:
      - name: nic0
        network: normal_net
```
