# Networks description

Primary intent of os_infra_manager is to create virtual machines as desired by
the person launching it.

But we need to know or create the networks that are necessary for these virtual
machines.

Therefore, a dictionary named `os_infra_server_name` is needed to describe the
network topology.

This dictionary is composed of keys being the network name and values being the
description of the network.

## Network description

Each network will be described with the following variables:

| Variable   | Purpose                                | Type    |
|------------|----------------------------------------|---------|
| `network`  | the network address                    | string  |
| `mask`     | the network mask                       | integer |
| `gateway`  | the network gateway                    | string  |
| `dns`      | the dns to use                         | string  |
| `internal` | does the network needs to be routed    | boolean |
| `provider` | does the network is a provided network | boolean |

`network` and `mask` are mandatory variables.
if `internal` or `provided` are not set, it's equivalent to `false`.

## Provider network

A provider network is a network that's not handled by OpenStack but by physical
switches / routers.
We support for the moment only provider network without DHCP.

## Examples

### Simplest example

this describe a network named `admin` with IP range being `192.168.1.0/24` and
gateway `192.168.1.1`.

```yaml
os_infra_net_config:
  admin:
    network: 192.168.1.0
    mask: 24
    gateway: 192.168.1.1
```

### More complex example

this describe three networks:

* one named `admin` with IP range being `192.168.1.0/24` and gateway
  `192.168.1.1`
* one named `provider_net` with IP range being `23.0.40.0/30` and gateway
  `23.0.40.1`. It's a provider network
* one named `internal_net` with IP range being `192.168.2.0/24` and no gateway

```yaml
os_infra_net_config:
  admin:
    network: 192.168.1.0
    mask: 24
    gateway: 192.168.1.1
  provider_net:
    network: 23.0.40.0
    mask: 30
    gateway: 23.0.40.1
    provider: true
  internal_net:
    network: 192.168.2.0
    mask: 24
    internal: true
```
