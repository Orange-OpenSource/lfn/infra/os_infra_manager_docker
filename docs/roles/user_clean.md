# Clean resources in a tenant

## Purpose

Clean resources in a tenant that follows a pattern.

All servers (and attached volumes), security groups which name finishes with
`os_infra_identifier` value will be deleted.

All networks ports which names finishes with `os_infra_network_identifier`
values will be deleted also.

## Parameters

<!-- markdownlint-disable line-length -->
| Variable                      | Purpose                                 | Default value                 |
|-------------------------------|-----------------------------------------|-------------------------------|
| `os_infra_identifier`         | identifier set on the resources         | `-by-os-infra-manager`        |
| `os_infra_network_identifier` | identifier set on the network resources | `"{{ os_infra_identifier }}"` |
| `os_infra_project_name`       | name of the project                     | Not set                       |
| `os_infra_user_name`          | name of the user                        | Not set                       |
<!-- markdownlint-enable line-length -->

`os_infra_project_name` and `os_infra_user_name` have no defaults but needs to
be provided (role will fail if not given).

## Examples

```yaml
- ansible.builtin.import_role:
    name: orange.os_infra_manager.user_clean
  vars:
    os_infra_project_name: my_project
    os_infra_user_name: my_user

- ansible.builtin.import_role:
    name: orange.os_infra_manager.user_clean
  vars:
    os_infra_project_name: my_project
    os_infra_user_name: my_user
    os_infra_identifier: -another-identifier
```
