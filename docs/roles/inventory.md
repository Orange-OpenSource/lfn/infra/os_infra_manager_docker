# Create inventory from a set of servers of a tenant

## Purpose

From a list of servers from OpenStack, create an inventory file and optionally
load them into the live inventory.

Also, create load balancers if needed.

## How to describe the nodes

In order to know the user to use, we retrieve the image type via the
[server_description](../generic/server_description.md).

## Default values

Per default, the servers description is the following:

```yaml
nodes:
  - name: server
    node:
      vendor: openstack
      model: debian-10
```

This means that the server "server" will use the default user from `debian-10`
image (`debian` per default).

Note that we'll look for all server with name finishing with the value from
`os_infra_identifier` variable.

If the image is not found or the user not found, user from
`os_infra_image2user_mapping` of `os_infra_image_default` variable value will be
used.

Unfortunately, ansible doesn't allow to configure values as dict with key being
a variable.
So if you change `os_infra_image_default`, you'll also need to update
`os_infra_image2user_mapping`.

per default, the server will be put in a group which is the value of
`os_infra_default_role` variable (`servers` per default).

Unfortunately, ansible doesn't allow to configure values as dict with key being
a variable.
So if you change `os_infra_server_name`, you'll also need to update
`os_infra_nodes_roles`.

## groups generation for inventory

ansible has several levels for groups:

* simple groups (underneath elements are servers)
* parent groups (underneath elements are groups)

In order to be able to do the same, this role provides two dictionaries:

* `os_infra_nodes_roles` will list all the roles per server
* `os_infra_roles_group` which will list all the sub roles of a role

Here's an example:

```yaml
os_infra_nodes_roles:
  server01:
    - db
    - prod
  server02:
    - http
    - prod
  server03:
    - db
    - staging
  server04:
    - staging

os_infra_roles_group:
  cluster:
    - db
    - http
  world:
    - cluster
    - prod
    - staging
```

It will generate this inventory (ini style):

```ini
[db]
server01
server03

[prod]
server01
server02

[http]
server02

[staging]
server03
server04

[cluster:children]
db
http

[world:children]
cluster
prod
staging
```

## DNS update

Once the inventory is generated, a dns record can be created.
It'll use IP address of `os_infra_dns_interface` value from
`os_infra_dns_master` value (for example `server` if using default description).

Currently, only Gandi DNS provider is supported.
you'll need to give an API token and set it into `gandiv5_api_key` variable.

## Parameters

<!-- markdownlint-disable line-length -->
| Variable                                    | Purpose                                                    | Default value                                               |
|---------------------------------------------|------------------------------------------------------------|-------------------------------------------------------------|
| `nodes`                                     | the servers description                                    | See [How to describe the nodes](#how-to-describe-the-nodes) |
| `os_infra_add_floating_ip`                  | look and add floating IP in the inventory                  | `true`                                                      |
| `os_infra_add_host_to_live_inventory`       | add the discovered servers into the live inventory         | `false`                                                     |
| `os_infra_default_role`                     | default role (group)                                       | `servers`                                                   |
| `os_infra_dns_update`                       | update DNS                                                 | `false`                                                     |
| `os_infra_dns_provider`                     | provider of the DNS                                        | Not Set                                                     |
| `os_infra_dns_zone`                         | DNS zone                                                   | Not Set                                                     |
| `os_infra_dns_name`                         | DNS name to use                                            | Not Set                                                     |
| `os_infra_dns_master`                       | server to use for setting the DNS                          | Not Set                                                     |
| `os_infra_dns_interface`                    | interface on the server to use for IP                      | `public`                                                    |
| `os_infra_identifier`                       | identifier set on the servers                              | `-by-os-infra-manager`                                      |
| `os_infra_image_default`                    | default image                                              | `"{{ os_infra_server_image }}"`                             |
| `os_infra_image_default_user`               | default user for default image                             | `debian`                                                    |
| `os_infra_image2user_mapping`               | mapping between image and user                             | `{os_infra_image_default: os_infra_image_default_user}`     |
| `os_infra_nodes_roles`                      | roles to be set for the servers                            | `{os_infra_server_name: [os_infra_default_role] }`          |
| `os_infra_roles_group`                      | parent roles to be set for the groups                      | `{}`                                                        |
| `os_infra_server_image`                     | image for the default server                               | `debian-10`                                                 |
| `os_infra_use_private_ip`                   | do we use the private ip in `ip` var in inventory          | `false`                                                     |
| `os_infra_load_balancers`                   | List of loadbalancers to create                            | Not Set                                                     |
| `os_infra_load_balancers_start_member_wait` | time to wait between pool and members creations            | `5` seconds                                                 |
| `os_infra_load_balancers_end_member_wait`   | time to wait between member creations                      | `5` seconds                                                 |
| `os_infra_load_balancers_member_wait`       | time to wait between members and health monitors creations | `3` seconds                                                 |
| `os_infra_load_balancers_monitor_wait`      | time to wait between health monitor creations              | `3` seconds                                                 |
| `gandiv5_api_key`                           | API key for GANDI DNS                                      | Not Set                                                     |
<!-- markdownlint-enable line-length -->

`os_infra_load_balancers` is not set. Look at
[load balancers descriptions](../generic/loadbalancers_description.md) for a
complete description.

## Examples

```yaml
- ansible.builtin.import_role:
    name: orange.os_infra_manager.flavor

- ansible.builtin.import_role:
    name: orange.os_infra_manager.configure_disks
  vars:
    os_infra_cloud_admin: my-admin
```
