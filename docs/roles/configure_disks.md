# Configure Linux mountpoints

## Purpose

From a list of disks, find, format and mount them to the right places.

Two lists are used:

* one (per server) describe the disks (volumes) present on the server
* the other list the mountpoints which will be looked at

## How to describe the disk

Disk description is done per server via the
[server_description](../generic/server_description.md).

The right disk list from a server will be found by checking that start of
inventory name of server is the same as `name` of node.

As each disk in this description has a name, this name is used to find the
"purpose" of the disk.
The role will look at all disks descriptions and see if one has its name ending
with the "purpose" part.
as said in [server_description](../generic/server_description.md), first disk
in the disk list is for root mount and then won't be used

Here's an example of list:

```yaml
nodes:
  - name: "{{ os_infra_server_name }}"
    disks:
      - name: disk1
        disk_capacity: 30G
        disk_type: qcow2
        disk_interface: ide
      - name: disk-log
        disk_capacity: 30G
        disk_type: qcow2
        disk_interface: ide
      - name: disk-var-log
        disk_capacity: 30G
        disk_type: qcow2
        disk_interface: ide
      - name: disk-home
        disk_capacity: 30G
        disk_type: qcow2
        disk_interface: ide
```

With this list:

* "purpose" `log` will match second volume
* "purpose" `var-log` will match third volume
* "purpose" `home` will match fourth volume
* "purpose" `kubernetes` won't match any volume

## Default values

### Disks description

Per default, the disk description is the following:

```yaml
nodes:
  - name: server
    disks:
      - name: disk1
        disk_capacity: 10G
        disk_type: qcow2
        disk_interface: ide
```

This means that no volume will be searched for.

### Mount description

By default, the role will look at the following disk:

* one named `os_infra_disks_home` with `home` purpose
* one named `os_infra_disks_var` with `var` purpose
* one named `os_infra_disks_var_log` with `logs` purpose
* one named `os_infra_disks_var_log_audit` with `logs-audit` purpose
* one named `os_infra_disks_var_tmp` with `var-tmp` purpose

this list can be changed via `os_infra_disks_create_list` value which is by
default:

```yaml
os_infra_disks_create_list:
  - os_infra_disks_var
  - os_infra_disks_var_log
  - os_infra_disks_home
  - os_infra_disks_var_tmp
```

Note that `os_infra_disks_var_log_audit` is not present as specific is needed on
it.

If you want to change the list, you'll need to update it and create a dictionary
per item with the following keys:

* `purpose`: the purpose of the disk
* `mount_path`: where to mount
* `force_full_erase`: do we wipe value in the volume before mounting
* `copy_data`: do we copy data from the mountpoint into the volume (if you want
  to mount a volume on an existing folder)
* `mount_options`: the options you want to use for mounting

For example, here's the one for `os_infra_disks_var_tmp`:

```yaml
os_infra_disks_var_tmp:
  purpose: var-tmp
  mount_path: /var/tmp
  force_full_erase: false
  copy_data: true
  mount_options: rw,noatime,nodiratime,noexec,nosuid,nodev,auto,async
```

If you want to add possibility to mount `/var/lib` for example, you'll need to
give the following (it's just an example):

```yaml
os_infra_disks_create_list:
  - os_infra_disks_var
  - os_infra_disks_var_log
  - os_infra_disks_home
  - os_infra_disks_var_tmp
  - os_infra_disks_var_lib

os_infra_disks_var_lib:
  purpose: var-lib
  mount_path: /var/lib
  force_full_erase: false
  copy_data: true
  mount_options: defaults
```

We have added `os_infra_disks_var_lib` in `os_infra_disks_create_list` list and
created a dictionary named `os_infra_disks_var_lib` with the right keys.

## Parameters

<!-- markdownlint-disable line-length -->
| Variable                                        | Purpose                                           | Default value                                             |
|-------------------------------------------------|---------------------------------------------------|-----------------------------------------------------------|
| `nodes`                                         | the servers description                           | See [How to describe the disk](#how-to-describe-the-disk) |
| `os_infra_disks_create_list`                    | list of mount path to search                      | See [Mount description](#mount-description)               |
| `os_infra_disks_home_purpose`                   | `os_infra_disks_home` purpose                     | `home`                                                    |
| `os_infra_disks_home_mount_path`                | `os_infra_disks_home` mount path                  | `/home`                                                   |
| `os_infra_disks_home_force_full_erase`          | `os_infra_disks_home` `force_full_erase`          | `false`                                                   |
| `os_infra_disks_home_copy_data`                 | `os_infra_disks_home` `copy_data`                 | `true`                                                    |
| `os_infra_disks_home_mount_options`             | `os_infra_disks_home` mount options               | `rw,noatime,nodiratime,nosuid,nodev,auto,async`           |
| `os_infra_disks_var_purpose`                    | `os_infra_disks_var` purpose                      | `var`                                                     |
| `os_infra_disks_var_mount_path`                 | `os_infra_disks_var` mount path                   | `/var`                                                    |
| `os_infra_disks_var_force_full_erase`           | `os_infra_disks_var` `force_full_erase`           | `false`                                                   |
| `os_infra_disks_var_copy_data`                  | `os_infra_disks_var` `copy_data`                  | `true`                                                    |
| `os_infra_disks_var_mount_options`              | `os_infra_disks_var` mount options                | `rw,noatime,nodiratime,nosuid,nodev,auto,async`           |
| `os_infra_disks_var_log_purpose`                | `os_infra_disks_var_log` purpose                  | `logs`                                                    |
| `os_infra_disks_var_log_mount_path`             | `os_infra_disks_var_log` mount path               | `/var/log`                                                |
| `os_infra_disks_var_log_force_full_erase`       | `os_infra_disks_var_log` `force_full_erase`       | `false`                                                   |
| `os_infra_disks_var_log_copy_data`              | `os_infra_disks_var_log` `copy_data`              | `true`                                                    |
| `os_infra_disks_var_log_mount_options`          | `os_infra_disks_var_log` mount options            | `rw,noatime,nodiratime,noexec,nosuid,nodev,auto,async`    |
| `os_infra_disks_var_log_audit_purpose`          | `os_infra_disks_var_log_audit` purpose            | `logs-audit`                                              |
| `os_infra_disks_var_log_audit_mount_path`       | `os_infra_disks_var_log_audit` mount path         | `/var/log/audit`                                          |
| `os_infra_disks_var_log_audit_force_full_erase` | `os_infra_disks_var_log_audit` `force_full_erase` | `false`                                                   |
| `os_infra_disks_var_log_audit_copy_data`        | `os_infra_disks_var_log_audit` `copy_data`        | `true`                                                    |
| `os_infra_disks_var_log_audit_mount_options`    | `os_infra_disks_var_log_audit` mount options      | `rw,noatime,nodiratime,noexec,nosuid,nodev,auto,async`    |
| `os_infra_disks_var_tmp_purpose`                | `os_infra_disks_var_tmp` purpose                  | `var-tmp`                                                 |
| `os_infra_disks_var_tmp_mount_path`             | `os_infra_disks_var_tmp` mount path               | `/var/tmp`                                                |
| `os_infra_disks_var_tmp_force_full_erase`       | `os_infra_disks_var_tmp` `force_full_erase`       | `false`                                                   |
| `os_infra_disks_var_tmp_copy_data`              | `os_infra_disks_var_tmp` `copy_data`              | `true`                                                    |
| `os_infra_disks_var_tmp_mount_options`          | `os_infra_disks_var_tmp` mount options            | `rw,noatime,nodiratime,noexec,nosuid,nodev,auto,async`    |
| `os_infra_server_name`                          | name of the default server                        | `server`                                                  |
<!-- markdownlint-enable line-length -->

## Examples

```yaml
- ansible.builtin.import_role:
    name: orange.os_infra_manager.configure_disks

- ansible.builtin.import_role:
    name: orange.os_infra_manager.configure_disks
  vars:
    os_infra_disks_home_purpose: "my-home"
    os_infra_disks_create_list:
      - os_infra_disks_var
      - os_infra_disks_var_log
      - os_infra_disks_home
      - os_infra_disks_var_tmp
      - os_infra_disks_var_lib
    os_infra_disks_var_lib:
      purpose: var-lib
      mount_path: /var/lib
      force_full_erase: false
      copy_data: true
      mount_options: defaults
```
