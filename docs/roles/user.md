# Create an user in OpenStack

## Purpose

Create an user in OpenStack.

An "Openstack RC" file will be created at `os_infra_openstack_user_openrc` place
on the Ansible controller with right values set.

## Parameters

<!-- markdownlint-disable line-length -->
| Variable                         | Purpose                                                | Default value                                 |
|----------------------------------|--------------------------------------------------------|-----------------------------------------------|
| `base_dir`                       | root folder of the collection                          | `"{{ inventory_dir }}/.."`                    |
| `os_infra_openstack_user_openrc` | path of openstack rc on controller for update          | `"{{ base_dir }}/vars/openstack_user_openrc"` |
| `os_infra_cloud_admin`           | name of "admin" credentials in OpenStack `clouds.yaml` | `admin`                                       |
| `os_infra_user_interface`        | name of user interface                                 | `public`                                      |
| `os_infra_user_domain_name`      | name of user domain name                               | `Default`                                     |
| `os_infra_project_domain_name`   | name of project domain name                            | `"{{ os_infra_user_domain_name }}"`           |
| `os_infra_identity_api_version`  | Keystone API version                                   | `"3"`                                         |
| `os_infra_region_name`           | OpenStack region name                                  | `RegionOne`                                   |
| `os_infra_user_password`         | password to set to the user                            | Not set                                       |
| `os_infra_project_name`          | name of the project                                    | Not set                                       |
| `os_infra_user_name`             | name of the user                                       | Not set                                       |
<!-- markdownlint-enable line-length -->

`os_infra_project_name` and `os_infra_user_name` have no defaults but needs to
be provided (role will fail if not given).

if `os_infra_user_password` is not provided, one will be automatically
generated.

## Examples

```yaml
# this will add the two keys as authorized keys on all servers
- ansible.builtin.import_role:
    name: orange.os_infra_manager.user
  vars:
    os_infra_project_name: my_project
    os_infra_user_name: my_user
```
