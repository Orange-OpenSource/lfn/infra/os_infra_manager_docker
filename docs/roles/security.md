# Create security resources

## Purpose

Create needed security resources (security group and keypair).

## Security group creation

Simple security groups and binding can be created with the following
limitations:

* if using a "remote group" as target, it's currently only possible to do so
  with security groups created by the same run.
  If you want to reach / be reached by servers others than the one in the group,
  you must give an IP prefix.
* the security groups will be applied to servers created during the run.
* no default rules are created so "egress allow all" will be there.

Security groups declaration is done in `os_infra_security_groups` variable which
is a list of security group

### Security group description

Each security group will be described with the following variables,
all mandatories:

| Variable      | Purpose                                          | Type     |
|---------------|--------------------------------------------------|----------|
| `suffix`      | suffix to set to default base name (can be `""`) | string   |
| `bindings`    | to which server roles we bind                    | string[] |
| `description` | a description that will be set                   | string   |
| `rules`       | the rules to create                              | rule[]   |

#### Binding the rules to the servers

The security group will be bound to all servers belonging to roles given in
bindings.

If you want to bind all the servers, you can use the special value `all`.

#### Examples

##### Simplest

```yaml
os_infra_security_groups:
  - suffix: ""
    bindings:
      - all
    description:
      "default security group for project {{ os_infra_project_name }}"
    rules: []

os_infra_nodes_roles:
  server_1:
    - role_1
    - role_2
  server_2:
    - role_1
```

The security group will be bound to servers `server_1` and `server_2`.

##### Some security groups to dedicated group

```yaml
os_infra_security_groups:
  - suffix: "http"
    bindings:
      - role_2
      - role_3
    description:
      "Allow HTTP"
    rules: []
  - suffix: "ssh"
    bindings:
      - all
    description:
      "Allow SSH"
    rules: []

os_infra_nodes_roles:
  server_1:
    - role_1
    - role_2
  server_2:
    - role_1
```

Security group `http` will only be bound to `server_1` whereas security group
`ssh` will be bound to both.

If other servers have `role_3`, they would also have security group `http`.

### Security group rules description

A security group can have several rules attached to it.

Each security group rule can be described with the following variables:

<!-- markdownlint-disable line-length -->
| Variable             | Purpose                           | Type             | Mandatory | Authorized values               |
|----------------------|-----------------------------------|------------------|-----------|---------------------------------|
| `direction`          | the direction for the rule        | string           | Yes       | `ingress` or `egress`           |
| `remote_groups`      | allowed remote groups             | string[]         | Yes       | can be `[]`                     |
| `remote_ip_prefixes` | allowed remote networks           | string[]         | Yes       | can be `[]`                     |
| `protocol`           | protocol used                     | string / integer | No        | See list underneath             |
| `port_range_min`     | min of the port range             | integer          | No        | `>=-1, <=65535`, `-1` means all |
| `port_range_max`     | max of the port range             | integer          | No        | `>=-1, <=65535`, `-1` means all |
<!-- markdownlint-enable line-length -->

**Explanations:**

* For a remote group, only the `suffix` needs to be given. It can be the same
  suffix as the one for the security group (to allow only members of the same
  security group)
* Remote IP prefixes shall be declared using the "CIDR" notation (example: `10.12.0.0/16`)
* at least one remote group OR (inclusive OR) one remote IP prefix needs to be
  given
* not setting `port_range_min` and `port_range_max` seems to be the same than
  setting them to `-1`.
* not setting a protocol means "any".

OpenStack documentation being pretty scarce (to say the least) on this subject,
please refer to
[Ansible module description examples](https://docs.ansible.com/ansible/latest/collections/openstack/cloud/security_group_rule_module.html#examples)
in order to see what can be done.

#### Protocol list

The following protocols are supported:

* ah
* dccp
* egp
* esp
* gre
* hopopt
* icmp
* igmp
* ip
* ipip
* ipv6-encap
* ipv6-frag
* ipv6-icmp
* icmpv6
* ipv6-nonxt
* ipv6-opts
* ipv6-route
* ospf
* pgm
* rsvp
* sctp
* tcp
* udp
* udplite
* vrrp

You can also give the protocol number (see
[the list of IP protocol numbers](https://en.wikipedia.org/wiki/List_of_IP_protocol_numbers))
to see what can be set.

#### Examples

##### Simplest

This is what is provided by default (allowing anyone to connect to the
server using any protocol):

```yaml
os_infra_security_groups:
  - suffix: ""
    bindings:
      - all
    description:
      "default security group for project {{ os_infra_project_name }}"
    rules:
      - remote_groups: []
        remote_ip_prefixes:
          - 0.0.0.0/0
        direction: ingress
```

#### VRRP in the same group

```yaml
os_infra_security_groups:
  - suffix: "vrrp"
    bindings:
      - loadbalancers
    description:
      "Allow VRRP"
    rules:
      - remote_groups:
          - vrrp
        remote_ip_prefixes: []
        protocol: 112
        direction: ingress
```

#### Allow SSH only from controllers

```yaml
os_infra_security_groups:
  - suffix: "ssh-controller"
    bindings:
      - worker
    description:
      "Allow SSH for controllers"
    rules:
      - remote_groups: []
        remote_ip_prefixes:
          - 0.0.0.0/0
        protocol: ssh
        direction: ingress
  - suffix: "ssh-worker"
    bindings:
      - worker
    description:
      "Allow SSH for workers"
    rules:
      - remote_groups:
          - ssh-controller
        remote_ip_prefixes: []
        protocol: ssh
        direction: ingress
```

## Public key retrieval

By default, this role will look for pub key of the user on the controller. If
not found, it will look at the one given in `vaulted_ssh_credentials`.

So `vaulted_ssh_credentials` is only needed when there's no key on ansible
controller (for example when on gitlab runner).

`vaulted_ssh_credentials` must have the key in the variable
`vault_ssh_id_rsa_pub`.

## Parameters

<!-- markdownlint-disable line-length -->
| Variable                               | Purpose                                  | Default value                                                                   |
|----------------------------------------|------------------------------------------|---------------------------------------------------------------------------------|
| `base_dir`                             | root folder of the collection            | `"{{ inventory_dir }}/.."`                                                      |
| `vaulted_ssh_credentials`              | where variable having ssh credentials is | `"{{ base_dir }}/vars/vaulted_ssh_credentials.yml"`                             |
| `os_infra_controller_ssh_pub_key_name` | name of pub key                          | `id_rsa.pub`                                                                    |
| `os_infra_controller_ssh_pub_key_path` | path of pub key                          | `"{{ lookup('env', 'HOME') }}/.ssh/{{ os_infra_controller_ssh_pub_key_name }}"` |
| `os_infra_identifier`                  | identifier set on the resources          | `-by-os-infra-manager`                                                          |
| `os_infra_project_name`                | name of the public network to use        | Not Set                                                                         |
| `os_infra_user_name`                   | name of the user                         | Not Set                                                                         |
<!-- markdownlint-enable line-length -->

`os_infra_project_name` and `os_infra_user_name` have no defaults but needs to
be provided (role will fail if not given).

## Examples

```yaml
- ansible.builtin.import_role:
    name: orange.os_infra_manager.security

- ansible.builtin.import_role:
    name: orange.os_infra_manager.security
  vars:
    os_infra_controller_ssh_pub_key_name: id_ed25519.pub
```
