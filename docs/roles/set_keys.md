# Add ssh public keys to servers

## Purpose

Add ssh public keys of authorized users to servers

## Parameters

| Variable                       | Purpose         | Default value |
|--------------------------------|-----------------|---------------|
| `os_infra_ssh_pub_key_default` | the keys to add | ""            |

## Examples

```yaml
# this will add the two keys as authorized keys on all servers
- ansible.builtin.import_role:
    name: orange.os_infra_manager.set_keys
  vars:
    os_infra_ssh_pub_key_default: >
      ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHNCB2C9i3TiiYMGjzPPIHY/Y714OotPNUxoMw
      Ci4tkk akey@key.com

      ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDGbXMXdrrZFPb+cOfZQ0orl1iL0xFR/Bp2lL
      +q7PZa+QXZWaSZi3je6CZjikaNIPUDa6T2xzaCnal8vZatjMLqMxV3KRjUrZlXry4amJ5VZXiV
      a/8qlHpJHjRlw1eMMBXwKO5XcZP5CRmRB+fyBrK+4x84Fs1X+oekQB5DwF/7QHCqW7MzXC2rDT
      h2WZ//2VDQ2r2VpFnbqo5nHrjQNMY5gbo8QJ11f/QMPimlplznPDKKHYxGgQQlB9Fc6TBJoceH
      erWpp1qTA2gWPOykjKJrhQscv7XpdARxXzmhg878GZqO6J+H8yKba62BR6EMu6F7PORBZtIM1Q
      bkMuk32xaz7E1LD7uq6SyCcNDtPocXUE03FIWBJoXWW4QM8Ibh+v9J0kRruTUwHg/vYEB8gFTM
      7ht00qU6QeGL2sh0RqzF4/1242Q2PWNgJuy+F8wSQE/Kj9gpBEOyZ8S4s14Mqt3rnAQC4DnnSI
      v4UuCLOe2fnXHAjeKUnBSjr0W/RKq2vc8= other@key.com
```
