# Create needed flavors

## Purpose

From a list of servers, create needed flavors

⚠️ You need to be admin in order to create flavors

## How to describe the nodes

Flavor are created with information from
[server_description](../generic/server_description.md).

## Default values

Per default, the servers description is the following:

```yaml
nodes:
  - name: server
    node:
      vendor: openstack
      cpus: 1
      memory: 1G
    disks: &disksControl
      - name: disk1
        disk_capacity: 10G
```

This means that a flavor with 1 vCPU, 1G of RAM and 10G of disk will be created.

If one flavor is sufficient, you can change the CPU, RAM and disk values via
the [parameters](#parameters) provided.

If not, you'll need to prove the right `nodes` description satisfying your
needs.

## Parameters

<!-- markdownlint-disable line-length -->
| Variable                 | Purpose                                                | Default value                                               |
|--------------------------|--------------------------------------------------------|-------------------------------------------------------------|
| `nodes`                  | the servers description                                | See [How to describe the nodes](#how-to-describe-the-nodes) |
| `os_infra_cloud_admin`   | name of "admin" credentials in OpenStack `clouds.yaml` | `admin`                                                     |
| `os_infra_server_cpus`   | number of vCPUs                                        | `1`                                                         |
| `os_infra_server_memory` | RAM size                                               | `1G`                                                        |
| `os_infra_server_disk`   | disk size                                              | `10G`                                                       |
<!-- markdownlint-enable line-length -->

## Examples

```yaml
- ansible.builtin.import_role:
    name: orange.os_infra_manager.flavor

- ansible.builtin.import_role:
    name: orange.os_infra_manager.flavor
  vars:
    os_infra_cloud_admin: my-admin
```
