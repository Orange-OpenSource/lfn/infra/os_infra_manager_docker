# Cloud file generator

## Purpose

OpenStack Cloud file generator.
Given variables to be set in it, generate (and merge with existing) cloud file
for OpenStack.

## Parameters

<!-- markdownlint-disable line-length -->
| Variable                                    | Purpose                                             | Default value                                                                                                  |
|---------------------------------------------|-----------------------------------------------------|----------------------------------------------------------------------------------------------------------------|
| `base_dir`                                  | root folder of the collection                       | `"{{ inventory_dir }}/.."`                                                                                     |
| `os_infra_cloud_auth_url`                   | url for authentication                              | Not set                                                                                                        |
| `os_infra_cloud_cacert`                     | ca file to use                                      | Not set                                                                                                        |
| `os_infra_cloud_compute_api_version`        | compute API version                                 | Not set                                                                                                        |
| `os_infra_cloud_identity_api_version`       | identity API version                                | `"3"`                                                                                                          |
| `os_infra_cloud_interface`                  | name of user interface                              | Not set                                                                                                        |
| `os_infra_cloud_password`                   | password of the user                                | Not set                                                                                                        |
| `os_infra_cloud_project_domain_id`          | domain id of the project                            | Not set                                                                                                        |
| `os_infra_cloud_project_domain_name`        | domain name of the project                          | Not set                                                                                                        |
| `os_infra_cloud_project_id`                 | id of the project                                   | Not set                                                                                                        |
| `os_infra_cloud_project_name`               | project name                                        | Not set                                                                                                        |
| `os_infra_cloud_region_name`                | region name                                         | `RegionOne`                                                                                                    |
| `os_infra_cloud_specific_ca_file`           | path and name where ca file will be store on target | `"{{ os_infra_openstack_config_folder }}/{{ os_infra_cloud_user_name }}_{{ os_infra_cloud_cacert|basename }}"` |
| `os_infra_cloud_user_domain_id`             | id of user domain                                   | `public`                                                                                                       |
| `os_infra_cloud_user_domain_name`           | name of user domain                                 | Not set                                                                                                        |
| `os_infra_cloud_user_name`                  | user name                                           | Not set                                                                                                        |
| `os_infra_local_cloud`                      | path of local cloud                                 | `"{{ base_dir }}/vars/user_cloud.yml"`                                                                         |
| `os_infra_openstack_config_folder`          | folder where OpenStack cloud file will be set       | `"{{ ansible_user_dir }}/.config/openstack"`                                                                   |
| `os_infra_openstack_config_file`            | name and path of OpenStack cloud file               | `"{{ os_infra_openstack_config_folder }}/clouds.yaml"`                                                         |
| `os_infra_openstack_cloud_config_lock_file` | name and path of OpenStack cloud lock file          | `"{{ os_infra_openstack_config_file }}.lock"`                                                                  |
| `os_infra_perform_cloud_vars_validation`    | do we validate the input                            | `True`                                                                                                         |
<!-- markdownlint-enable line-length -->

### Mandatory variables

#### When user cloud file exists

role will just load the content of the file and merge into
`os_infra_openstack_config_file`.

So no variable is needed as long as the file is present

#### When user cloud file doesn't exists

The following variables are needed in order to construct the file:

* `os_infra_cloud_auth_url`
* `os_infra_cloud_interface`
* `os_infra_cloud_password`
* `os_infra_cloud_project_name`
* `os_infra_cloud_user_name`

### Specific CA

If access to OpenStack API needs a specific CA, it must be given on the Ansible
controller (where we run the playbook).

The path will be `{{ base_dir }}/{{ os_infra_cloud_cacert }}`. This specific
file will be copied in `{{ os_infra_cloud_specific_ca_file }}` place on the
target and cloud config file will have it configured this way.

## Examples

```yaml
# when `os_infra_local_cloud` exists
- ansible.builtin.import_role:
    name: orange.os_infra_manager.cloud

# when `os_infra_local_cloud` doesn't exists
- ansible.builtin.import_role:
    name: orange.os_infra_manager.cloud
  vars:
    os_infra_project_name: my_project
    os_infra_user_name: my_user
    os_infra_cloud_auth_url: http://1.2.3.4:5000/
    os_infra_cloud_interface: public
    os_infra_cloud_password: s3cr3t

- ansible.builtin.import_role:
    name: orange.os_infra_manager.cloud
  vars:
    os_infra_project_name: my_project
    os_infra_user_name: my_user
    os_infra_cloud_auth_url: http://1.2.3.4:5000/
    os_infra_cloud_interface: public
    os_infra_cloud_password: s3cr3t
    os_infra_cloud_cacert: vars/ca.crt

- ansible.builtin.import_role:
    name: orange.os_infra_manager.cloud
  vars:
    os_infra_project_name: my_project
    os_infra_user_name: my_user
    os_infra_cloud_auth_url: http://1.2.3.4:5000/
    os_infra_cloud_interface: public
    os_infra_cloud_password: s3cr3t
    os_infra_cloud_compute_api_version: "2.97"
```
