# Create servers

## Purpose

Create needed servers and associated resources (volumes and floating IPs if
needed).

The servers are described thanks to
[server_description](../generic/server_description.md).

The networks are described thanks to
[networks_description](../generic/networks_description.md).

## Default values

### Default values for servers

Per default, the servers description is the following:

```yaml
nodes:
  - name: server
    node:
      vendor: openstack
      model: debian-10
```

All these values (name, model) are overidable by [parameters](#parameters).

If you want more servers, you'll need to override `nodes` with desired
description.

### Default values for networks

Per default, the networks description is the following:

```yaml
os_infra_net_config:
  admin:
    network: 10.253.0.0
    mask: 24
    gateway: 10.253.0.1
    dns: 10.253.0.1
```

Values can be overriden by [parameters](#parameters) except network name.

If you want more networks or a different name, you'll need to override
`os_infra_net_config` with desired description.

if `os_infra_public_net_name`, role will try to find a suitable provider
network.

Unfortunately, ansible doesn't allow to configure values as dict with key being
a variable.
So if you change `os_infra_server_name`, you'll also need to update
`os_infra_nodes_roles`.

### Allowing IP Pairs

When on provider network, you can allow supplemental IP Pairs to a port.

Here's an example on what to add:

```yaml
os_infra_allowed_ip_pairs:
  - group: kube-node
    pairs:
      - ip_address: 1.2.3.4
      - ip_address: 5.6.7.8
    nic: nic0
```

## Parameters

<!-- markdownlint-disable line-length -->
| Variable                                             | Purpose                                      | Default value                                                   |
|------------------------------------------------------|----------------------------------------------|-----------------------------------------------------------------|
| `os_infra_project_name`                              | name of the project                          | Not set                                                         |
| `os_infra_user_name`                                 | name of the user                             | Not set                                                         |
| `os_infra_add_floating_ip`                           | add floating IP on servers                   | `true`                                                          |
| `os_infra_availability_zone`                         | availibity zone to use                       | `nova`                                                          |
| `os_infra_identifier`                                | identifier set on the resources              | `-by-os-infra-manager`                                          |
| `os_infra_image_default`                             | default image to use                         | debian-10                                                       |
| `os_infra_network_identifier`                        | identifier set on the network resources      | `"{{ os_infra_identifier }}"`                                   |
| `os_infra_nodes`                                     | list of servers to create                    | `"{{ nodes }}"`                                                 |
| `os_infra_boot_from_volume`                          | do we start from a volume or not not         | `false`                                                         |
| `os_infra_public_net_name`                           | public network to use for floating IP        | Not Set                                                         |
| `os_infra_admin_network_network`                     | network address for default network          | `10.253.0.0`                                                    |
| `os_infra_admin_network_mask`                        | mask for default network                     | `24`                                                            |
| `os_infra_admin_network_gateway`                     | gateway for default network                  | First IP of `os_infra_admin_network_network`                    |
| `os_infra_admin_network_dns`                         | DNS for default network                      | `"{{ os_infra_admin_network_gateway }}"`                        |
| `os_infra_server_name`                               | name of default server                       | `server`                                                        |
| `os_infra_server_cpus`                               | groups for servers                           | `1`                                                             |
| `os_infra_server_memory`                             | name of the user                             | `1G`                                                            |
| `os_infra_server_disk`                               | name of the user                             | `10G`                                                           |
| `os_infra_default_role`                              | default role for the server                  | `servers`                                                       |
| `os_infra_nodes_roles`                               | mapping of roles per servers                 | `{ 'server': [{{ os_infra_default_role }}]}`                    |
| `os_infra_net_config`                                | network configuration description            | See [Default values for networks](#default-values-for-networks) |
| `nodes`                                              | list of all servers                          | See [Default values for servers](#default-values-for-servers)   |
| `os_infra_allowed_ip_pairs`                          | Allowed IP Pairs configuration               | See [Allowing IP Pairs](#allowing-ip-pairs)                     |
| `os_infra_centos_repository_baseos_url`              | BaseOs repo override for Centos              | Not Set                                                         |
| `os_infra_centos_repository_appstream_url`           | AppStream repo override for Centos           | Not Set                                                         |
| `os_infra_centos_repository_extras_url`              | Extras repo override for Centos              | Not Set                                                         |
| `os_infra_ubuntu_repository_disable_suites`          | To disable entries in sources list           | Not Set                                                         |
| `os_infra_ubuntu_repository_primary_archive_mirror`  | URL of the primary archive mirror            | Not Set                                                         |
| `os_infra_ubuntu_repository_security_archive_mirror` | URL of the security archive mirror           | Not Set                                                         |
| `os_infra_ubuntu_repository_sources_list`            | Custom template for sources.list             | Not Set                                                         |
| `os_infra_ubuntu_apt_conf`                           | Specify configuration for apt                | Not Set                                                         |
| `os_infra_ubuntu_repository_sources`                 | Dictionary to specify sources.list.d entries | Not Set                                                         |

<!-- markdownlint-enable line-length -->

`os_infra_user_name` and `os_infra_project_nameè has no default but needs to be
provided (role will fail if not given).

## Examples

```yaml
- ansible.builtin.import_role:
    name: orange.os_infra_manager.servers

- ansible.builtin.import_role:
    name: orange.os_infra_manager.servers
  vars:
    os_infra_identifier: -my-identifier
    os_infra_server_name: my-server
```
