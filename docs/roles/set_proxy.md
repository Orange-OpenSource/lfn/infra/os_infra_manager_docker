# Add proxies on servers

## Purpose

if proxy variables are given, configure the servers to be used

## Parameters

| Variable                  | Purpose                         | Default value |
|---------------------------|---------------------------------|---------------|
| `os_infra_vm_http_proxy`  | HTTP proxy                      | Not set       |
| `os_infra_vm_https_proxy` | HTTPS proxy                     | Not set       |
| `os_infra_vm_no_proxy`    | Hosts list which we don't proxy | Not set       |
| `python_pip_repository`   | PIP repository                  | Not set       |
| `python_pip_trusted_host` | Trusted hosts for PIP           | Not set       |

`os_infra_vm_no_proxy` follows `no_proxy` use in Linux, meaning that CIDR
networks are not working.

## Examples

```yaml
# this will configure proxy part and no proxy for ip addresses in 10.1.2.0 to
# 10.1.2.3
- ansible.builtin.import_role:
    name: orange.os_infra_manager.set_proxy
  vars:
    os_infra_vm_http_proxy: http://1.2.3.4:3218/
    os_infra_vm_https_proxy: http://1.2.3.4:3218/
    os_infra_no_proxy: .infra,10.1.2.0,10.1.2.1,10.1.2.2,10.1.2.3
```
