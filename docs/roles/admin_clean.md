# Administrator Clean

## Purpose

Remove all objects from an OpenStack project and remove this project and the
user.

## Parameters

<!-- markdownlint-disable line-length -->
| Variable                       | Purpose                                                | Default value                       |
|--------------------------------|--------------------------------------------------------|-------------------------------------|
| `os_infra_cloud_admin`         | name of "admin" credentials in OpenStack `clouds.yaml` | `admin`                             |
| `os_infra_user_domain_name`    | name of user domain name                               | `Default`                           |
| `os_infra_project_domain_name` | name of project domain name                            | `"{{ os_infra_user_domain_name }}"` |
| `os_infra_user_interface`      | name of user interface                                 | `public`                            |
| `os_infra_project_name`        | name of the project                                    | Not set                             |
| `os_infra_user_name`           | name of the user                                       | Not set                             |
<!-- markdownlint-enable line-length -->

`os_infra_project_name` and `os_infra_user_name` have no defaults but needs to
be provided (role will fail if not given).

## Examples

```yaml
- ansible.builtin.import_role:
    name: orange.os_infra_manager.admin_clean
  vars:
    os_infra_project_name: my_project
    os_infra_user_name: my_user
```
