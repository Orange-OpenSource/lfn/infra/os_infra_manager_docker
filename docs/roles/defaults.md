# Load project default values and can perform full validation

## Purpose

Load project default values.

It can also perform the full schema validation (instead of verifying for each
steps)

## Parameters

All default values are described in the roles using them

<!-- markdownlint-disable no-inline-html line-length -->
| Variable                                    | Purpose                                 | Default value |
|---------------------------------------------|-----------------------------------------|---------------|
| `os_infra_perform_per_role_vars_validation` | will we perform role based validation?  | `true`        |
| `os_infra_perform_full_vars_validation`     | will we perform full schema validation? | `false`       |
<!-- markdownlint-enable no-inline-html line-length -->
