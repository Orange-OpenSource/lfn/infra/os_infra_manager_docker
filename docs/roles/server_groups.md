# Create server groups

## Purpose

Create needed server groups.

os infra manager needs to create server groups as they're called when creating
servers.

This role will create a server group per different group given in
`os_infra_nodes_roles`.

per default, `os_infra_nodes_roles` is set to this:

```yaml
os_infra_nodes_roles:
  server: [servers]
```

## Parameters

<!-- markdownlint-disable line-length -->
| Variable                      | Purpose                         | Default value           |
|-------------------------------|---------------------------------|-------------------------|
| `os_infra_identifier`         | identifier set on the resources | `-by-os-infra-manager`  |
| `os_infra_nodes_group_policy` | policy to use on server groups  | `soft-anti-affinity`    |
| `os_infra_nodes_roles`        | groups for servers              | See [Purpose](#purpose) |
| `os_infra_user_name`          | name of the user                | Not Set                 |
<!-- markdownlint-enable line-length -->

`os_infra_user_name` has no default but needs to be provided (role will fail if
not given).

## Examples

```yaml
- ansible.builtin.import_role:
    name: orange.os_infra_manager.security

- ansible.builtin.import_role:
    name: orange.os_infra_manager.security
  vars:
    os_infra_controller_ssh_pub_key_name: id_ed25519.pub
```
