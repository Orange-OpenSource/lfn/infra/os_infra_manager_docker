# Create needed project

## Purpose

Create needed project and assign user to it.

## Default values for role assignement

roles for user and admin to be assigned are given via `os_infra_user_roles` and
`os_infra_admin_roles` lists.

here's the default lists:

```yaml
os_infra_user_roles:
  - _member_
  - heat_stack_owner
  - load-balancer_member

os_infra_admin_roles:
  - _member_
  - admin
  - heat_stack_owner
```

## Default value for role creation

Some roles needs to be present and sometimes are not there.
`os_infra_roles_to_create` var ensures they're present.

Here's the default value:

```yaml
os_infra_roles_to_create:
  - heat_stack_owner
  - load-balancer_member
```

## RBAC for accessing specific public network

Some public networks may have limited access and a rbac needs to be created for
them.

In order to do that, `os_infra_project_public_net_limited` must be set to `true`
and `os_infra_project_public_net_name` must be provided.

## Parameters

Quotas are set for 1 instance and 1 network. If you plan for more, you need to
update accordingly.

Please note that some values are dynamic and depends of other values so you
shouldn't have to change them most of the time.

<!-- markdownlint-disable line-length -->
| Variable                                       | Purpose                                                | Default value                                 |
|------------------------------------------------|--------------------------------------------------------|-----------------------------------------------|
| `base_dir`                                     | root folder of the collection                          | `"{{ inventory_dir }}/.."`                    |
| `openstack_user_openrc`                        | path of openstack rc on controller for update          | `"{{ base_dir }}/vars/openstack_user_openrc"` |
| `os_infra_cloud_admin`                         | name of "admin" credentials in OpenStack `clouds.yaml` | `admin`                                       |
| `os_infra_project_public_net_limited`          | public network is limited and RBAC has to be set       | `false`                                       |
| `os_infra_project_public_net_name`             | public network name                                    | Not Set                                       |
| `os_infra_project_quotas_cores`                | quota for CPU cores                                    | `2`                                           |
| `os_infra_project_quotas_ram`                  | quota for RAM                                          | `2048` (Mb)                                   |
| `os_infra_project_quotas_gigabytes`            | quota for disk size                                    | `"20` (Gb)                                    |
| `os_infra_project_quotas_floating_ips`         | quota for floating IP                                  | 1 per instances                               |
| `os_infra_project_quotas_instances`            | quota for servers                                      | `1`                                           |
| `os_infra_project_quotas_volumes`              | quota for volumes                                      | `0`                                           |
| `os_infra_project_quotas_snapshots`            | quota for volume snapshots                             | 5 per instances                               |
| `os_infra_project_quotas_rbac_policy`          | quota for RBAC policies                                | 1 per network                                 |
| `os_infra_project_quotas_networks`             | quota for networks                                     | `1`                                           |
| `os_infra_project_quotas_ports`                | quota for network ports                                | (2 per networks + 1 per instances) * 2        |
| `os_infra_project_quotas_subnets`              | quota for subnets                                      | 1 per network                                 |
| `os_infra_project_quotas_security_group`       | quota for security groups                              | `10`                                          |
| `os_infra_project_quotas_security_group_rule`  | quota for security group rules                         | `10`                                          |
| `os_infra_project_quotas_routers`              | quota for routers                                      | `1`                                           |
| `os_infra_project_quotas_server_group_members` | quota for server group members                         | `1`                                           |
| `os_infra_project_quotas_server_groups`        | quota for server groups                                | `10`                                          |

<!-- markdownlint-enable line-length -->

## Examples

```yaml
- ansible.builtin.import_role:
    name: orange.os_infra_manager.project

- ansible.builtin.import_role:
    name: orange.os_infra_manager.project
  vars:
    os_infra_project_quotas_instances: 2
```
