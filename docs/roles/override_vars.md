# Override user and project variable

## Purpose

Helper role used in playbooks to override user and project name.

`os_infra_user_name` will be set to `os_infra_override_user_name` if this one is
defined and not empty

Same for `os_infra_project_name` and `os_infra_override_project_name`.

This is used in playbooks to overrides these two vars via environnment variables
(when you just want to change these two fields).

## Examples

```yaml
- ansible.builtin.import_role:
    name: orange.os_infra_manager.override_vars
```
