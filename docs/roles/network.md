# Create needed networks

## Purpose

Create needed networks and routers.

## How to describe the networks

networks are created with information from
[networks_description](../generic/networks_description.md).

## Default values

Per default, the networks description is the following:

```yaml
os_infra_net_config:
  admin:
    network: 10.253.0.0
    mask: 24
    gateway: 10.253.0.1
    dns: 10.253.0.1
```

Values can be overriden by [parameters](#parameters).

If you need only one "normal" network, you don't have to provide a full
`os_infra_net_config` but only overrides inner values if needed.
If you need more or provider network, you'll have to override.

When network needs to be binded to an external network (for access out of
OpenStack), you can either provide it via `os_infra_public_net_name` or it will be
automatically retrieved by choosing first provider network.

SNAT enable on router needs admin rights if not set by default.

## Parameters

<!-- markdownlint-disable line-length -->
| Variable                         | Purpose                                      | Default value                                                     |
|----------------------------------|----------------------------------------------|-------------------------------------------------------------------|
| `base_dir`                       | root folder of the collection                | `"{{ inventory_dir }}/.."`                                        |
| `os_infra_admin_network_network` | default network address                      | `10.253.0.0`                                                      |
| `os_infra_admin_network_mask`    | default network mask                         | `24`                                                              |
| `os_infra_admin_network_gateway` | default network gateway                      | 1 IP of network                                                   |
| `os_infra_admin_network_dns`     | default network dns                          | `"{{ os_infra_admin_network_gateway }}"`                          |
| `os_infra_enable_snat`           | SNAT enable on router                        | `true`                                                            |
| `os_infra_identifier`            | identifier set on the resources              | `-by-os-infra-manager`                                            |
| `os_infra_net_config`            | networks description                         | See [How to describe the networks](#how-to-describe-the-networks) |
| `os_infra_network_identifier`    | identifier set on the network resources      | `"{{ os_infra_identifier }}"`                                     |
| `os_infra_project_infos`         | files where network infos will be written on | `"{{ base_dir }}/vars/project_infos.yml"`                         |
| `os_infra_project_name`          | name of the project                          | Not Set                                                           |
| `os_infra_public_net_name`       | name of the public network to use            | Not Set                                                           |
| `os_infra_user_name`             | name of the user                             | Not Set                                                           |
<!-- markdownlint-enable line-length -->

`os_infra_project_name` and `os_infra_user_name` have no defaults but needs to
be provided (role will fail if not given).

## Examples

```yaml
- ansible.builtin.import_role:
    name: orange.os_infra_manager.network

- ansible.builtin.import_role:
    name: orange.os_infra_manager.configure_disks
  vars:
    os_infra_admin_network_network: "192.168.3.0"
```
