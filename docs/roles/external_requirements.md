# Install needed external requirements

## Purpose

Install external requirements needed by the other roles of this collection.

## Parameters

<!-- markdownlint-disable no-inline-html line-length -->
| Variable                | Purpose                     | Default value              |
|-------------------------|-----------------------------|----------------------------|
| `python_pip_repository` | which pip repository to use | `https://pypi.org/simple/` |
<!-- markdownlint-enable no-inline-html line-length -->

proxies are retrieved via their respective value in `ansible_env` variable also.

## Examples

```yaml
- ansible.builtin.import_role:
    name: orange.os_infra_manager.external_requirements

- ansible.builtin.import_role:
    name: orange.os_infra_manager.external_requirements
  vars:
    python_pip_repository: http://myproxy/simple
```
