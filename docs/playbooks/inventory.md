# Inventory generation playbook

## Purpose

Generate Ansible compliant inventory file.

## Inventory

The inventory must provide a group `jumphost` with **single host** in it.

See [role parameters](../roles/inventory.md) for all override paramaters that
can be set.

⚠️ both property files `idf.yml` and `pdf.yml` here can be retrieved here.

Search for property and infrastructure files (`idf.yml` and `pdf.yml` per
default) happens at the same level as inventory directory (see optional
parameters to see how to change) per default:

```shell
.
├── inventory
│   └── inventory
└── vars
    ├── idf.yml
    └── pdf.yml
```

## Mandatory parameters

None

## Optional parameters

See [default values in inventory](./default_values_in_inventory.md) on how to
some variables are overriden when `inventory` file is in `inventory` folder.

On top of all variables from roles, the playbook adds the following variables:

<!-- markdownlint-disable no-inline-html line-length -->
| Variable              | Purpose                      | Default value            |
|-----------------------|------------------------------|--------------------------|
| `property_file`       | property file to load        | `idf.yml`. Optional      |
| `infrastructure_file` | infrastructure file to load  | `pdf.yml`. Optional      |
| `base_dir`            | folder where is file to load | `{{ inventory_dir }}/..` |
<!-- markdownlint-enable no-inline-html line-length -->

## Tags

The playbook proposes the following tags:

| Tag                          | Purpose                          |
|------------------------------|----------------------------------|
| `os_infra_manager_inventory` | for controlling inventory launch |

## Examples

Inventory:

```ini
[jumphost]
my_server ansible_host: 1.2.3.4 ansible_user: toto
```

Launch:

```shell
ansible-playbook -i inventory/inventory playbooks/inventory.yml

ansible-playbook -i inventory/inventory playbooks/inventory.yml \
  --tags os_infra_manager_inventory

ansible-playbook -i inventory/inventory playbooks/inventory.yml \
  --extra-vars "base_dir=~"
```
