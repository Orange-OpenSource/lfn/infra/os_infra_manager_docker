# Configure disks on virtual machines playbook

## Purpose

configure disks (format, mount them at right place) on servers.

## Inventory

The run will be done on all hosts except servers part of `jumphost` group.

See [role parameters](../roles/configure_disks.md) for all override paramaters
that can be set.

⚠️ The default property file for search is `pdf.yml` here.

Search for vars files (`pdf.yml` per default) happens at the same level as
inventory directory (see optional parameters to see how to change) per default:

```shell
.
├── inventory
│   └── inventory
└── vars
    └── pdf.yml
```

## Mandatory parameters

None

## Optional parameters

On top of all variables from roles, the playbook adds the following variables:

| Variable        | Purpose                      | Default value            |
|-----------------|------------------------------|--------------------------|
| `property_file` | property file to load        | `pdf.yml`. Optional      |
| `base_dir`      | folder where is file to load | `{{ inventory_dir }}/..` |

## Tags

The playbook proposes the following tags:

| Tag                                | Purpose                                |
|------------------------------------|----------------------------------------|
| `os_infra_manager_configure_disks` | for controlling configure_disks launch |

## Examples

Inventory:

```ini
[servers]
my_server ansible_host: 1.2.3.4 ansible_user: toto
```

Launch:

```shell
ansible-playbook -i inventory/inventory playbooks/configure_disks.yml

ansible-playbook -i inventory/inventory playbooks/configure_disks.yml \
  --tags os_infra_manager_configure_disks

ansible-playbook -i inventory/inventory playbooks/configure_disks.yml \
  --extra-vars "base_dir=~"
```
