# Default values in `inventory`

If `inventory` is set in `inventory` folder, several default values will be
overriden.

there are 4 variable categories:

## Hardening parameters

<!-- markdownlint-disable no-inline-html line-length -->
| Variable                              | Purpose                         | Default value                                           |
|---------------------------------------|---------------------------------|---------------------------------------------------------|
| `os_infra_ssh_permit_tunnel`          | permit ssh tunneling            | `true`                                                  |
| `os_infra_ssh_allow_tcp_forwarding`   | allow tcp forwarding            | `"yes"`                                                 |
| `os_infra_sysctl_overwrite`           | sysctl overrided configuration  | `{ net.ipv4.ip_forward: 1}`                             |
<!-- markdownlint-enable no-inline-html line-length -->

Please be careful and see that `os_infra_ssh_allow_tcp_forwarding` value is not
a boolean but the string `yes` as it will be put "as is" into sshd configuration
file. So values are either `yes` or `no` for this one.

In the contrary, `os_infra_ssh_permit_tunnel` is a boolean and must be set with
`true` or `false`.

## user and project override parameters

They're used in all playbook where project and user name are needed and will
override `os_infra_user_name` or `os_infra_project_name` if set.

<!-- markdownlint-disable no-inline-html line-length -->
| Variable                              | Purpose                         | Default value                                           |
|---------------------------------------|---------------------------------|---------------------------------------------------------|
| `os_infra_override_user_name`         | override value for user name    | `USER_NAME` env value                                   |
| `os_infra_override_project_name`      | override value for project name | `PROJECT_NAME` or `TENANT_NAME` env value               |
<!-- markdownlint-enable no-inline-html line-length -->

## Cloud parameters

they're usually retrieved from an OpenStack Open RC file.

<!-- markdownlint-disable no-inline-html line-length -->
| Variable                              | Purpose                         | Default value                                           |
|---------------------------------------|---------------------------------|---------------------------------------------------------|
| `os_infra_cloud_region_name`          | cloud region name               | `OS_REGION_NAME` env value                              |
| `os_infra_cloud_project_domain_name`  | cloud project domain name       | `OS_PROJECT_DOMAIN_NAME` env value                      |
| `os_infra_cloud_project_domain_id`    | cloud project domain id         | `OS_PROJECT_DOMAIN_ID` env value                        |
| `os_infra_cloud_user_name`            | cloud user name                 | `OS_USERNAME` env value                                 |
| `os_infra_cloud_password`             | cloud password                  | `OS_PASSWORD` env value                                 |
| `os_infra_cloud_project_name`         | cloud project name              | `OS_PROJECT_NAME` env value                             |
| `os_infra_cloud_project_id`           | cloud project id                | `OS_PROJECT_ID` or `OS_TENANT_ID` env value             |
| `os_infra_cloud_interface`            | cloud interface                 | `OS_INTERFACE` env value                                |
| `os_infra_cloud_auth_url`             | cloud auth url                  | `OS_AUTH_URL` env value                                 |
| `os_infra_cloud_user_domain_name`     | cloud user domain name          | `OS_USER_DOMAIN_NAME` env value                         |
| `os_infra_cloud_user_domain_id`       | cloud user domain id            | `OS_USER_DOMAIN_ID` env value                           |
| `os_infra_cloud_identity_api_version` | cloud identity API version      | `OS_IDENTITY_API_VERSION` env value                     |
| `os_infra_cloud_compute_api_version`  | cloud compute API version       | `OS_COMPUTE_API_VERSION` env value or default to `2.95` |
| `os_infra_cloud_cacert`               | cloud CA certificate path       | `OS_CACERT` env value                                   |
<!-- markdownlint-enable no-inline-html line-length -->

## Gandi DNS variable

They're used for using GANDI DNS API (in `inventory` playbook).

<!-- markdownlint-disable no-inline-html line-length -->
| Variable                              | Purpose                         | Default value                                           |
|---------------------------------------|---------------------------------|---------------------------------------------------------|
| `gandiv5_api_key`                     | GANDI v5 API key                | `GANDI_API_V5_KEY` env value                            |
<!-- markdownlint-enable no-inline-html line-length -->
