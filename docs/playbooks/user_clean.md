# User clean playbook

## Purpose

Remove objects from an OpenStack project matching format.

## Inventory

The inventory must provide a group `jumphost` with **single host** in it. This
is a group of server that is able to connect to OpenStack API.

A prerequisite is there's a `clouds.yml` file available on the server with an
user account. By default, it's retrieved via `USER_NAME` environment variable.

See [role parameters](../roles/user_clean.md) for all override paramaters that
can be set.

Search for vars files (`idf.yml` per default) happens at the same level as
inventory directory (see optional parameters to see how to change) per default:

```shell
.
├── inventory
│   └── inventory
└── vars
    └── idf.yml
```

## Mandatory parameters

None

## Optional parameters

See [default values in inventory](./default_values_in_inventory.md) on how to
some variables are overriden when `inventory` file is in `inventory` folder.

On top of all variables from roles, the playbook adds the following variables:

| Variable        | Purpose                      | Default value            |
|-----------------|------------------------------|--------------------------|
| `property_file` | property file to load        | `idf.yml`. Optional      |
| `base_dir`      | folder where is file to load | `{{ inventory_dir }}/..` |

## Tags

The playbook proposes the following tags:

| Tag                            | Purpose                            |
|--------------------------------|------------------------------------|
| `os_infra_manager_user_clean` | for controlling admin clean launch |

## Examples

Inventory:

```ini
[jumphost]
my_server ansible_host: 1.2.3.4 ansible_user: toto
```

Launch:

```shell
ansible-playbook -i inventory/inventory playbooks/user_clean.yml

ansible-playbook -i inventory/inventory playbooks/user_clean.yml \
  --tags os_infra_manager_user_clean

ansible-playbook -i inventory/inventory playbooks/user_clean.yml \
  --extra-vars "base_dir=~"
```
