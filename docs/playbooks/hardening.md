# Hardening servers playbook

## Purpose

Launch server hardening on servers.

## Inventory

The run will be done on all hosts except servers part of `jumphost` group.

See [devsec hardening collection](https://galaxy.ansible.com/devsec/hardening)
for all override paramaters that can be set (`os_hardening` and `ssh_hardening`
roles are used).

## Mandatory parameters

None

## Optional parameters

See [default values in inventory](./default_values_in_inventory.md) on how to
some variables are overriden when `inventory` file is in `inventory` folder.

In particular, here are the one for hardening paremeters:

<!-- markdownlint-disable no-inline-html line-length -->
| Variable                            | Purpose                        | Default value               |
|-------------------------------------|--------------------------------|-----------------------------|
| `os_infra_ssh_permit_tunnel`        | permit ssh tunneling           | `true`                      |
| `os_infra_ssh_allow_tcp_forwarding` | allow tcp forwarding           | `"yes"`                     |
| `os_infra_sysctl_overwrite`         | sysctl overrided configuration | `{ net.ipv4.ip_forward: 1}` |
<!-- markdownlint-enable no-inline-html line-length -->

On top of all variables from roles, the playbook adds the following variables:

<!-- markdownlint-disable no-inline-html line-length -->
| Variable                   | Purpose                 | Default value                               |
|----------------------------|-------------------------|---------------------------------------------|
| `ssh_permit_tunnel`        | permit ssh tunneling    | `"{{ os_infra_ssh_permit_tunnel }}"`        |
| `ssh_allow_tcp_forwarding` | allow tcp forwarding    | `"{{ os_infra_ssh_allow_tcp_forwarding }}"` |
| `sysctl_overwrite`         | sysctl overrided config | `"{{ os_infra_sysctl_overwrite }}"`         |
<!-- markdownlint-enable no-inline-html line-length -->

## Tags

None

## Examples

Inventory:

```ini
[servers]
my_server ansible_host: 1.2.3.4 ansible_user: toto
```

Launch:

```shell
ansible-playbook -i inventory/inventory playbooks/hardening.yml

ansible-playbook -i inventory/inventory playbooks/hardening.yml \
  --extra-vars "base_dir=~"
```
