# Set key on virtual machines playbook

## Purpose

Set ssh authorized keys on servers.

## Inventory

The run will be done on all hosts except servers part of `jumphost` group.

See [role parameters](../roles/set_keys.md) for all override paramaters
that can be set.

Search for vars files (`idf.yml` per default) happens at the same level as
inventory directory (see optional parameters to see how to change) per default:

```shell
.
├── inventory
│   └── inventory
└── vars
    └── idf.yml
```

## Mandatory parameters

None

## Optional parameters

On top of all variables from roles, the playbook adds the following variables:

| Variable        | Purpose                      | Default value            |
|-----------------|------------------------------|--------------------------|
| `property_file` | property file to load        | `idf.yml`. Optional      |
| `base_dir`      | folder where is file to load | `{{ inventory_dir }}/..` |

## Tags

The playbook proposes the following tags:

| Tag                         | Purpose                         |
|-----------------------------|---------------------------------|
| `os_infra_manager_set_keys` | for controlling set_keys launch |

## Examples

Inventory:

```ini
[servers]
my_server ansible_host: 1.2.3.4 ansible_user: toto
```

Launch:

```shell
ansible-playbook -i inventory/inventory playbooks/set_keys.yml

ansible-playbook -i inventory/inventory playbooks/set_keys.yml \
  --tags os_infra_manager_set_keys

ansible-playbook -i inventory/inventory playbooks/set_keys.yml \
  --extra-vars "base_dir=~"
```
