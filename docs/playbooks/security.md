# Security resources creation playbook

## Purpose

Create OpenStack security resources.

## Inventory

The inventory must provide a group `jumphost` with **single host** in it.

See [role parameters](../roles/security.md) for all override paramaters that
can be set.

Search for vars files (`idf.yml` per default) happens at the same level as
inventory directory (see optional parameters to see how to change) per default:

```shell
.
├── inventory
│   └── inventory
└── vars
    └── idf.yml
```

## Mandatory parameters

None

## Optional parameters

See [default values in inventory](./default_values_in_inventory.md) on how to
some variables are overriden when `inventory` file is in `inventory` folder.

On top of all variables from roles, the playbook adds the following variables:

| Variable        | Purpose                      | Default value            |
|-----------------|------------------------------|--------------------------|
| `property_file` | property file to load        | `idf.yml`. Optional      |
| `base_dir`      | folder where is file to load | `{{ inventory_dir }}/..` |

## Tags

The playbook proposes the following tags:

| Tag                         | Purpose                         |
|-----------------------------|---------------------------------|
| `os_infra_manager_security` | for controlling security launch |

## Examples

Inventory:

```ini
[jumphost]
my_server ansible_host: 1.2.3.4 ansible_user: toto
```

Launch:

```shell
ansible-playbook -i inventory/inventory playbooks/security.yml

ansible-playbook -i inventory/inventory playbooks/security.yml \
  --tags os_infra_manager_security

ansible-playbook -i inventory/inventory playbooks/security.yml \
  --extra-vars "base_dir=~"
```
